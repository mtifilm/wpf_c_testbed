﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ModalDialogManager.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>rancor\pfirth</author>
// <date>1/15/2013 11:15:06 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;
    using System.Timers;
    using System.Windows;
    using System.Windows.Threading;
    using MtiSimple.Core;

    using Timer = System.Timers.Timer;

    /// <summary>
    /// Class that defines a modal dialog manager.
    /// </summary>
    public class ModalDialogManager : IDisposable
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The default center point X.
        /// </summary>
        public const int DefaultCenterPointX = 1920 / 2;

        /// <summary>
        /// The default center point y.
        /// </summary>
        public const int DefaultCenterPointY = 1080 / 2;

        /// <summary>
        /// The default minimum display time in milliseconds.
        /// </summary>
        private const int DefaultMinimumDisplayTimeInMilliseconds = 1000;

        /// <summary>
        /// The pending dialog queue
        /// </summary>
        private static readonly Queue<ModalDialogManager> PendingDialogQueue = new Queue<ModalDialogManager>();

        /// <summary>
        /// A lock to make global state checking and changing operations atomic.
        /// </summary>
        private static readonly object GlobalStateLock = new object();

        /// <summary>
        /// The last active manager.
        /// </summary>
        private static ModalDialogManager lastActiveDialog;

        /// <summary>
        /// A lock to make instance state checking and changing operations atomic.
        /// </summary>
        private readonly object instanceStateLock = new object();

        /// <summary>
        /// The delay timer
        /// </summary>
        private readonly Timer delayTimer;

        /// <summary>
        /// The modal dialog type
        /// </summary>
        private readonly Type modalDialogType;

        /// <summary>
        /// The display timer
        /// </summary>
        private readonly Stopwatch displayTimer;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The modal dialog thread
        /// </summary>
        private Thread modalDialogThread;

        /// <summary>
        /// The modal dialog
        /// </summary>
        private Window modalDialog;

        /// <summary>
        /// This gets set to true when the thread is completely initialized and is about to be shown.
        /// </summary>
        private bool isThreadInitComplete;

        /// <summary>
        /// The minimum display time
        /// </summary>
        private TimeSpan minimumDisplayTime;

        /// <summary>
        /// The dialog state.
        /// </summary>
        private DialogStates dialogState;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="ModalDialogManager"/> class.
        /// </summary>
        static ModalDialogManager()
        {
            DialogCenterPoint = new Point(DefaultCenterPointX, DefaultCenterPointY);
        }

        /// <summary>
        /// Initializes a new instance of the ModalDialogManager class
        /// </summary>
        /// <param name="delay">The delay.</param>
        /// <param name="minimumDisplayTime">The minimum display time.</param>
        /// <param name="dialogWindowType">The dialog window type.</param>
        /// <param name="dataContext">The data context.</param>
        public ModalDialogManager(TimeSpan delay, TimeSpan minimumDisplayTime, Type dialogWindowType, BindableObject dataContext = null)
        {
            this.dialogState = DialogStates.Pending;
            this.BindingSource = dataContext;
            this.modalDialogType = dialogWindowType;
            this.delayTimer = new Timer(delay.TotalMilliseconds);
            this.delayTimer.Elapsed += this.HandleDelayTimerElapsedEvent;
            this.delayTimer.Start();
            this.minimumDisplayTime = minimumDisplayTime;
            this.displayTimer = new Stopwatch();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModalDialogManager" /> class.
        /// </summary>
        /// <param name="dialogWindowType">Type of the dialog window.</param>
        /// <param name="dataContext">The data context.</param>
        public ModalDialogManager(Type dialogWindowType, BindableObject dataContext = null)
            : this(TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(DefaultMinimumDisplayTimeInMilliseconds), dialogWindowType, dataContext)
        {
        }

        #endregion

        //////////////////////////////////////////////////
        // Enums
        //////////////////////////////////////////////////
        #region Enums

        /// <summary>
        /// The three stages of the modal dialogs lifetime.
        /// </summary>
        private enum DialogStates
        {
            /// <summary>
            /// Waiting to be displayed; either it's still in the initial delay period or
            /// it was queued because another dialog was active when the delay ended. 
            /// </summary>
            Pending,

            /// <summary>
            /// the dialog is currently active,
            /// </summary>
            Active,

            /// <summary>
            /// The dialog was disposed.
            /// </summary>
            Disposed
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets a value indicating whether the modal dialog is active (may not be visible yet).
        /// </summary>
        public static bool IsDialogActive { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the modal dialog is visible.
        /// </summary>
        public static bool IsDialogVisible { get; private set; }

        /// <summary>
        /// Gets or sets the main window center. Used to position the modal dialog.
        /// </summary>
        public static Point DialogCenterPoint { get; set; }

        /// <summary>
        /// Gets the binding source.
        /// </summary>
        public BindableObject BindingSource { get; private set; }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.delayTimer.Stop();

            lock (this.instanceStateLock)
            {
                var oldDialogState = this.dialogState;
                this.dialogState = DialogStates.Disposed;
                if (oldDialogState == DialogStates.Disposed || oldDialogState == DialogStates.Pending)
                {
                    // Either we've been through here before, or we were canceled before becoming active.
                    // In the latter case we might be in the "pending" queue, but we'll just let that
                    // get cleaned up later.
                    return;
                }
            }

            // We are the active dialog.
            var nextDialog = (ModalDialogManager)null;
            lock (GlobalStateLock)
            {
                // See if there's another dialog to show.
                while (PendingDialogQueue.Count > 0)
                {
                    nextDialog = PendingDialogQueue.Dequeue();
                    lock (nextDialog.instanceStateLock)
                    {
                        if (nextDialog.dialogState == DialogStates.Pending)
                        {
                            // Found a pending dialog that wasn't canceled.
                            IsDialogActive = true;
                            lastActiveDialog = nextDialog;
                            nextDialog.dialogState = DialogStates.Active;
                        }
                        else
                        {
                            nextDialog = null;
                        }
                    }
                }

                if (nextDialog == null)
                {
                    // Nothing to activate.
                    IsDialogActive = false;
                }
            }

            // Show the next dialog, if any (outside the lock).
            if (nextDialog != null)
            {
                nextDialog.ShowDialog();
            }

            if (this.modalDialogThread != null)
            {
                lock (this.instanceStateLock)
                {
                    if (!this.isThreadInitComplete)
                    {
                        return;
                    }
                }

                var dispatcher = Dispatcher.FromThread(this.modalDialogThread);
                if (dispatcher != null)
                {
                    dispatcher.Invoke(
                        DispatcherPriority.Background,
                        new Action(
                            () =>
                                {
                                    // Only enforce minimum display time if there wasn't another dialog to activate,
                                    if (!IsDialogActive)
                                    {
                                        this.displayTimer.Stop();
                                        var remainingTimeToDisplay = this.minimumDisplayTime.Milliseconds - (int)this.displayTimer.ElapsedMilliseconds;
                                        if (remainingTimeToDisplay > 0)
                                        {
                                            Thread.Sleep(remainingTimeToDisplay);
                                        }
                                    }

                                    this.modalDialog.Close();
                                }));
                }
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Handles the delay timer elapsed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        private void HandleDelayTimerElapsedEvent(object sender, ElapsedEventArgs e)
        {
            this.delayTimer.Stop();
            lock (this.instanceStateLock)
            {
                if (this.dialogState == DialogStates.Disposed)
                {
                    return;
                }

                lock (GlobalStateLock)
                {
                    if (IsDialogActive)
                    {
                        // A dialog is already active, so queue this request.
                        PendingDialogQueue.Enqueue(this);
                        return;
                    }

                    IsDialogActive = true;
                    lastActiveDialog = this;
                }

                this.dialogState = DialogStates.Active;
            }

            this.ShowDialog();
        }

        /// <summary>
        /// Shows the modal dialog.
        /// </summary>
        private void ShowDialog()
        {
            // Create the dialog on background thread.
            this.modalDialogThread = new Thread(
                () =>
                    {
                        // Bail out if the dialog is no longer wanted.
                        if (this.dialogState == DialogStates.Disposed)
                        {
                            return;
                        }

                        // Create our context, and install it:
                        SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));

                        this.modalDialog = (Window)Activator.CreateInstance(this.modalDialogType);
                        this.modalDialog.DataContext = this.BindingSource;

                        this.modalDialog.WindowStartupLocation = WindowStartupLocation.Manual;
                        this.modalDialog.Left = DialogCenterPoint.X - (modalDialog.Width / 2);
                        this.modalDialog.Top = DialogCenterPoint.Y - (modalDialog.Height / 2);

                        lock (this.instanceStateLock)
                        {
                            // One last check before we make the window visible.
                            if (this.dialogState == DialogStates.Disposed)
                            {
                                this.modalDialog.Close();
                                this.modalDialog = null;
                                return;
                            }

                            // When the window closes, shut down the dispatcher
                            this.modalDialog.Closed += (s, e) =>
                            {
                                // HACK  sometimes lastActiveDialog != this, hanging the main GUI
                                ////if (lastActiveDialog == this)
                                ////{
                                    IsDialogVisible = false;
                                ////}

                                Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
                            };

                            // It is now safe to invoke this thread from the main thread.
                            this.isThreadInitComplete = true;
                        }

                        IsDialogVisible = true;
                        this.modalDialog.Show();

                        this.displayTimer.Start();

                        Dispatcher.Run();
                    });

            // Set the apartment state
            this.modalDialogThread.SetApartmentState(ApartmentState.STA);

            // Make the thread a background thread
            this.modalDialogThread.IsBackground = true;

            // Start the thread
            this.modalDialogThread.Start();
        }

        #endregion
    }
}
