﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArrowCreater.cs" company="MTI Film LLC">
// Copyright (c) 2017 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\hans.lehmann</author>
// <date>4/12/2017 5:09:57 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------
namespace MtiFilm.Control.Wpf.NumericUpDownTextBox
{
    using System;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Arrow Creater
    /// </summary>
    public class ArrowCreater : IMultiValueConverter
    {
        /// <summary>
        /// Converts source values to a value for the binding target. The data binding engine calls this method when it propagates the values from source bindings to the binding target.
        /// </summary>
        /// <param name="values">The array of values that the source bindings in the <see cref="T:System.Windows.Data.MultiBinding" /> produces. The value <see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the source binding has no value to provide for conversion.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value.If the method returns null, the valid null value is used.A return value of <see cref="T:System.Windows.DependencyProperty" />.<see cref="F:System.Windows.DependencyProperty.UnsetValue" /> indicates that the converter did not produce a value, and that the binding will use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> if it is available, or else will use the default value.A return value of <see cref="T:System.Windows.Data.Binding" />.<see cref="F:System.Windows.Data.Binding.DoNothing" /> indicates that the binding does not transfer the value or use the <see cref="P:System.Windows.Data.BindingBase.FallbackValue" /> or the default value.
        /// </returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var width = (double)values[0];
            var height = (double)values[1];
            
            if (height.Equals(0.0) || width.Equals(0.0)) 
            {
                return null;
            }

            var borderThickness = (Thickness)values[2];
            var up = (bool)values[3];
            var arrowHeight = height - borderThickness.Top - borderThickness.Bottom;
            var arrowWidth = width - borderThickness.Left - borderThickness.Right;
            return this.CreateArrow(arrowWidth, arrowHeight, up);
        }

        /// <summary>
        /// Converts a binding target value to the source binding values.
        /// </summary>
        /// <param name="value">The value that the binding target produces.</param>
        /// <param name="targetTypes">The array of types to convert to. The array length indicates the number and types of values that are suggested for the method to return.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// An array of values that have been converted from the target value back to the source values.
        /// </returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates the arrow.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="isUp">if set to <c>true</c> [is up].</param>
        /// <returns>the arrow</returns>
        private PointCollection CreateArrow(double width, double height, bool isUp)
        {
            // Bit of a hack here.  If button width is greater then height, orientation is assumed to be vertical,
            // else orientation is horizontal
            if (width > height)
            {
                var margin = height * .2;
                double pointY;
                double baseY;
                if (isUp)
                {
                    pointY = margin;
                    baseY = height - margin;
                }
                else
                {
                    baseY = margin;
                    pointY = height - margin;
                }

                var pts = new PointCollection 
                { 
                    new Point(margin, baseY), 
                    new Point(width / 2, pointY), 
                    new Point(width - margin, baseY) 
                };

                return pts;
            }
            else
            {
                var margin = width * .2;
                double pointX;
                double baseX;
                if (isUp)
                {
                    pointX = width - margin;
                    baseX = margin;
                }
                else
                {
                    baseX = width - margin;
                    pointX = margin;
                }

                var pts = new PointCollection 
                { 
                    new Point(baseX, margin), 
                    new Point(pointX, height / 2), 
                    new Point(baseX, height - margin) 
                };

                return pts;
            }
        }
    }
}
