﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NumericUpDownTextBox.xaml.cs" company="Mti Film, LLC">
// Copyright (c) 2017 by MTI Film, All Rights Reserved
// </copyright>
// <summary>
//   Defines the NumericUpDownTextBox type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf.NumericUpDownTextBox
{
    using System;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Interaction logic for NumericUpDownTextBox.xaml
    /// </summary>    
    public partial class NumericUpDownTextBox
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The maximum value property
        /// </summary>
        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register(
            "MaxValue",
            typeof(int),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(int.MaxValue, MaxValueChangedCallback));

        /// <summary>
        /// The minimum value property
        /// </summary>
        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register(
            "MinValue",
            typeof(int),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(0, MinValueChangedCallback));

        /// <summary>
        /// The value property
        /// </summary>
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register(
            "Value",
            typeof(int),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(0, ValueChangedCallback));

        /// <summary>
        /// The button background property
        /// </summary>
        public static readonly DependencyProperty ButtonBackgroundProperty =
            DependencyProperty.Register(
            "ButtonBackground",
            typeof(Brush),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The button foreground property
        /// </summary>
        public static readonly DependencyProperty ButtonForegroundProperty =
            DependencyProperty.Register(
            "ButtonForeground",
            typeof(Brush),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The button pressed background property
        /// </summary>
        public static readonly DependencyProperty ButtonPressedBackgroundProperty =
            DependencyProperty.Register(
            "ButtonPressedBackground",
            typeof(Brush),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The button mouse over background property
        /// </summary>
        public static readonly DependencyProperty ButtonMouseOverBackgroundProperty =
            DependencyProperty.Register(
            "ButtonMouseOverBackground",
            typeof(Brush),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The button border brush property
        /// </summary>
        public static readonly DependencyProperty ButtonBorderBrushProperty =
            DependencyProperty.Register(
            "ButtonBorderBrush",
            typeof(Brush),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The button border thickness property
        /// </summary>
        public static readonly DependencyProperty ButtonBorderThicknessProperty =
            DependencyProperty.Register(
            "ButtonBorderThickness",
            typeof(Thickness?),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));

        /// <summary>
        /// The direction property
        /// </summary>
        public static readonly DependencyProperty DirectionProperty =
            DependencyProperty.Register(
            "Direction",
            typeof(SpinBoxDirection),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(SpinBoxDirection.Normal));

        /// <summary>
        /// The button orientation property
        /// </summary>
        public static readonly DependencyProperty ButtonOrientationProperty =
            DependencyProperty.Register(
            "ButtonOrientation",
            typeof(Orientation),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(Orientation.Vertical));

        /// <summary>
        /// The button corner radius property
        /// </summary>
        public static readonly DependencyProperty ButtonCornerRadiusProperty =
            DependencyProperty.Register(
            "ButtonCornerRadius",
            typeof(CornerRadius?),
            typeof(NumericUpDownTextBox),
            new UIPropertyMetadata(null, ButtonPropertyChangedCallback));
        
        /// <summary>
        /// The controls
        /// </summary>
        private readonly VisualCollection controls;

        /// <summary>
        /// Up button
        /// </summary>
        private readonly RepeatButton upbutton;

        /// <summary>
        /// Down button
        /// </summary>
        private readonly RepeatButton downButton;

        /// <summary>
        /// The buttons view model
        /// </summary>
        private readonly ButtonsProperties buttonsViewModel;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// Set to indicate if the Buttons are displayed.
        /// The buttons are not displayed if the width to height 
        /// ratio of the control is less than a certain value
        /// </summary>
        private bool showButtons = true;
        
        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="NumericUpDownTextBox"/> class.
        /// </summary>
        public NumericUpDownTextBox()
        {
            this.InitializeComponent();
            var buttons = new ButtonsProperties(this);
            this.buttonsViewModel = buttons;

            // Create buttons
            this.upbutton = new RepeatButton
            {
                Focusable = false,
                Cursor = Cursors.Arrow,
                DataContext = buttons,
                Tag = true
            };
            
            this.upbutton.Click += this.OnUpButtonClick;

            this.downButton = new RepeatButton
            {
                Focusable = false,
                Cursor = Cursors.Arrow,
                DataContext = buttons,
                Tag = false
            };
            
            this.downButton.Click += this.OnDownButtonClick;

            // Create control collections
            this.controls = new VisualCollection(this) { this.upbutton, this.downButton };

            // Hook up text event handlers
            this.PreviewTextInput += this.OnControlPreviewTextInput;
            this.PreviewKeyDown += this.OnControlPreviewKeyDown;
            this.LostFocus += this.OnControlLostFocus;
            this.MouseWheel += this.OnControlMouseWheel;
            this.Text = this.Value.ToString();
        }
        #endregion

        /// <summary>
        /// determines whether up/right button increments or decrements the value
        /// </summary>
        public enum SpinBoxDirection
        {
            /// <summary>
            /// Up/Right arrow increments that value
            /// </summary>
            Normal,

            /// <summary>
            /// Down/Left arrow increments that value
            /// </summary>
            Inverted
        }

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the maximum value.
        /// </summary>
        /// <value>
        /// The maximum value.
        /// </value>
        public int MaxValue
        {
            get 
            { 
                return (int)this.GetValue(MaxValueProperty); 
            }

            set 
            { 
                this.SetValue(MaxValueProperty, value); 
            }
        }
        
        /// <summary>
        /// Gets or sets the minimum value.
        /// </summary>
        public int MinValue
        {
            get 
            { 
                return (int)this.GetValue(MinValueProperty); 
            }

            set 
            { 
                this.SetValue(MinValueProperty, value); 
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public int Value
        {
            get => (int)this.GetValue(ValueProperty);
            set => this.SetValue(ValueProperty, value);
        }

        /// <summary>
        /// Gets or sets the direction.
        /// </summary>
        public SpinBoxDirection Direction
        {
            get
            {
                return (SpinBoxDirection)this.GetValue(DirectionProperty);
            }

            set
            {
                this.SetValue(DirectionProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button orientation.
        /// </summary>
        public Orientation ButtonOrientation
        {
            get
            {
                return (Orientation)this.GetValue(ButtonOrientationProperty);
            }

            set
            {
                this.SetValue(ButtonOrientationProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button background.
        /// </summary>
        public Brush ButtonBackground
        {
            get
            {
                return (Brush)this.GetValue(ButtonBackgroundProperty);
            }

            set
            {
                this.SetValue(ButtonBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button foreground.
        /// </summary>
        public Brush ButtonForeground
        {
            get
            {
                return (Brush)this.GetValue(ButtonForegroundProperty);
            }

            set
            {
                this.SetValue(ButtonForegroundProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button pressed background.
        /// </summary>
        public Brush ButtonPressedBackground
        {
            get
            {
                return (Brush)this.GetValue(ButtonPressedBackgroundProperty);
            }

            set
            {
                this.SetValue(ButtonPressedBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button mouse over background.
        /// </summary>
        public Brush ButtonMouseOverBackground
        {
            get
            {
                return (Brush)this.GetValue(ButtonMouseOverBackgroundProperty);
            }

            set
            {
                this.SetValue(ButtonMouseOverBackgroundProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button border brush.
        /// </summary>
        public Brush ButtonBorderBrush
        {
            get
            {
                return (Brush)this.GetValue(ButtonBorderBrushProperty);
            }

            set
            {
                this.SetValue(ButtonBorderBrushProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button border thickness.
        /// </summary>
        public Thickness? ButtonBorderThickness
        {
            get
            {
                return (Thickness?)this.GetValue(ButtonBorderThicknessProperty);
            }

            set
            {
                this.SetValue(ButtonBorderThicknessProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the button corner radius.
        /// </summary>
        public CornerRadius? ButtonCornerRadius
        {
            get
            {
                return (CornerRadius?)this.GetValue(ButtonCornerRadiusProperty);
            }

            set
            {
                this.SetValue(ButtonCornerRadiusProperty, value);
            }
        }

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get
            {
                if (this.showButtons)
                {
                    return this.controls.Count + base.VisualChildrenCount;
                }

                return base.VisualChildrenCount;
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Called to arrange and size the content of a 
        ///         System.Windows.Controls.Control object.
        /// </summary>
        /// <param name="arrangeSize">The computed size that is used to 
        ///                 arrange the content</param>
        /// <returns>The size of the control</returns>
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            var height = arrangeSize.Height;
            var width = arrangeSize.Width;
            this.showButtons = width > 1.5 * height;

            if (this.showButtons)
            {
                double buttonWidth, buttonsLeft;
                Size buttonSize, textBoxSize;
                Rect downButtonRect, upbuttonRect;
                switch (this.ButtonOrientation)
                {
                    case Orientation.Vertical:
                        buttonWidth = 3 * height / 4;
                        buttonSize = new Size(buttonWidth, height / 2);
                        textBoxSize = new Size(width - buttonWidth, height);
                        buttonsLeft = width - buttonWidth;
                        upbuttonRect = new Rect(new Point(buttonsLeft, 0), buttonSize);
                        downButtonRect = new Rect(new Point(buttonsLeft, height / 2), buttonSize);
                        base.ArrangeOverride(textBoxSize);
                        this.upbutton.Arrange(upbuttonRect);
                        this.downButton.Arrange(downButtonRect);
                        return arrangeSize;

                    case Orientation.Horizontal:
                        buttonWidth = height / 2;
                        buttonSize = new Size(buttonWidth, height);
                        textBoxSize = new Size(width - (2 * buttonWidth), height);
                        buttonsLeft = width - (2 * buttonWidth);
                        downButtonRect = new Rect(new Point(buttonsLeft, 0), buttonSize);
                        buttonsLeft += buttonWidth;
                        upbuttonRect = new Rect(new Point(buttonsLeft, 0), buttonSize);
                        base.ArrangeOverride(textBoxSize);
                        this.downButton.Arrange(downButtonRect);
                        this.upbutton.Arrange(upbuttonRect);
                        return arrangeSize;
                }
            }

            return base.ArrangeOverride(arrangeSize);
        }

        /// <summary>
        /// Overrides System.Windows.Media.Visual.GetVisualChild(System.Int32), and returns
        /// a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>The requested child element.</returns>
        protected override Visual GetVisualChild(int index)
        {
            if (index < base.VisualChildrenCount)
            {
                return base.GetVisualChild(index);
            }

            return this.controls[index - base.VisualChildrenCount];
        }

        /// <summary>
        /// Handles changes of Value
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as NumericUpDownTextBox;
            control?.ValueChangedCallback((int)e.OldValue, (int)e.NewValue);
        }

        /// <summary>
        /// Handles changes of MinValue
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MinValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as NumericUpDownTextBox;
            control?.MinValueChangedCallback();
        }

        /// <summary>
        /// handles changes of MaxValue
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MaxValueChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as NumericUpDownTextBox;
            if (control == null)
            {
                return;
            }

            control.MaxValueChangedCallback();
        }
        
        /// <summary>
        /// Buttons the property changed callback.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void ButtonPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            NumericUpDownTextBox target = (NumericUpDownTextBox)d;
            target.buttonsViewModel.NotifyPropertyChanged(e.Property.ToString());
        }

        /// <summary>
        /// Called when [up button click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OnUpButtonClick(object sender, RoutedEventArgs e)
        {
            this.HandleModifiers(1);
        }

        /// <summary>
        /// Called when [down button click].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OnDownButtonClick(object sender, RoutedEventArgs e)
        {
            this.HandleModifiers(-1);
        }

        /// <summary>
        /// Called when [control preview text input].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="TextCompositionEventArgs"/> instance containing the event data.</param>
        private void OnControlPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // Catch any non-numeric keys
            if ("0123456789".IndexOf(e.Text, StringComparison.Ordinal) < 0)
            {
                // else check for negative sign
                if (e.Text == "-" && this.MinValue < 0)
                {
                    if (this.Text.Length == 0 || (this.CaretIndex == 0 &&
                                    this.Text[0] != '-'))
                    {
                        e.Handled = false;
                        return;
                    }
                }

                e.Handled = true;
            }
            else
            {
                // A digit has been pressed
                // We now know that have good value: check for attempting to put number before '-'
                if (this.Text.Length > 0 && this.CaretIndex == 0 &&
                    this.Text[0] == '-' && this.SelectionLength == 0)
                {
                    // Can't put number before '-'
                    e.Handled = true;
                }
                else
                {
                    // check for what new value will be:
                    var sb = new StringBuilder(this.Text);
                    sb.Remove(this.CaretIndex, this.SelectionLength);
                    sb.Insert(this.CaretIndex, e.Text);
                    int newValue = int.Parse(sb.ToString());
                    
                    // check if beyond allowed values
                    if (this.FixValueKeyPress(newValue))
                    {
                        e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        /// <summary>
        /// Called when [control preview key down].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        private void OnControlPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                this.HandleModifiers(-1);
                e.Handled = true;
            }
            else if (e.Key == Key.Up)
            {
                this.HandleModifiers(1);
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                // Space key is not caught by PreviewTextInput
                e.Handled = true;
            }
            else if (e.Key == Key.Enter || e.Key == Key.Escape)
            {
                int outValue;
                if (int.TryParse(this.Text, out outValue))
                {
                    this.Value = outValue;
                }

                Keyboard.ClearFocus();
                e.Handled = false;
            }
            else
            {
                e.Handled = false;
            }
        }

        /// <summary>
        /// Called when [control lost focus].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void OnControlLostFocus(object sender, RoutedEventArgs e)
        {
            int outValue;
            if (int.TryParse(this.Text, out outValue))
            {
                this.Value = outValue;
            }

            this.FixValue();
        }

        /// <summary>
        /// Called when [control mouse wheel].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseWheelEventArgs"/> instance containing the event data.</param>
        private void OnControlMouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.Add(e.Delta);
        }

        /// <summary>
        /// Checks if any of the Keyboard modifier keys are pressed that might 
        /// affect the change in the value of the TextBox.
        /// In this case only the shift key affects the value
        /// </summary>
        /// <param name="value">Integer value to modify</param>
        private void HandleModifiers(int value)
        {
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                value *= 10;
            }

            this.Add(value);
        }

        /// <summary>
        /// Add specified value the TextBox, fixing value
        /// if less than Minimum or greater than manimum
        /// </summary>
        /// <param name="value">Interger to add to TextBox value</param>
        private void Add(int value)
        {
            int currentValue;
            if (int.TryParse(this.Text, out currentValue))
            {
                currentValue += this.Direction == SpinBoxDirection.Normal ? value : -value;
                if (this.FixValue(currentValue))
                {
                    this.UpdateText(currentValue);
                }
            }
            else if (string.IsNullOrWhiteSpace(this.Text) && value != 0)
            {
                if (this.FixValue(value))
                {
                    this.UpdateText(value);
                }
            }
        }

        /// <summary>
        /// Only does something if the Textbox contains an out of range number
        /// </summary>
        private void FixValue()
        {
            int value;
            if (int.TryParse(this.Text, out value))
            {
                this.FixValue(value);
            }
        }

        /// <summary>
        /// This only does something if the value is out of range
        /// (ie above maximum or below minimum allowed values
        /// </summary>
        /// <param name="value">interger to check against limits</param>
        /// <returns>true iff nothing to fix</returns>
        private bool FixValue(int value)
        {
            if (value > this.MaxValue)
            {
                this.UpdateText(this.MaxValue);
                return false;
            }

            if (value < this.MinValue)
            {
                this.UpdateText(this.MinValue);
                return false;
            }

            // else: nothing to fix
            return true;
        }

        /// <summary>
        /// Fixes the value key press.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>true iff nothing to fix</returns>
        private bool FixValueKeyPress(int value)
        {
            // Want to allow user to build a number with keystrokes
            if (value > this.MaxValue)
            {
                this.UpdateText(this.MaxValue);
                return false;
            }

            if (value < this.MinValue)
            {
                this.UpdateText(this.MinValue);
                return false;
            }

            // else: nothing to fix
            return true;
        }

        /// <summary>
        /// Updates the text.
        /// </summary>
        /// <param name="value">The value.</param>
        private void UpdateText(int value)
        {
            this.Text = value.ToString();
            this.CaretIndex = this.Text.Length;
            this.Value = value;
        }

        /// <summary>
        /// Values the changed callback.
        /// </summary>
        /// <param name="oldValue">The old value.</param>
        /// <param name="newValue">The new value.</param>
        // ReSharper disable once UnusedParameter.Local
        private void ValueChangedCallback(int oldValue, int newValue)
        {
            this.Text = newValue.ToString();
        }

        /// <summary>
        /// Maximums the value changed callback.
        /// </summary>
        private void MaxValueChangedCallback()
        {
            this.FixValue();
        }

        /// <summary>
        /// Minimums the value changed callback.
        /// </summary>
        private void MinValueChangedCallback()
        {
            this.FixValue();
        }

        #endregion
    }
}