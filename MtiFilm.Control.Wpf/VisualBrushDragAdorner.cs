﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VisualBrushDragAdorner.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>5/26/2016 11:17:50 AM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;
    using System.Windows.Shapes;

    /// <summary>
    /// Class that defines the visual brush drag adorner.
    /// </summary>
    public class VisualBrushDragAdorner : Adorner
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The x center.
        /// </summary>
        private readonly double centerX;

        /// <summary>
        /// The y center.
        /// </summary>
        private readonly double centerY;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////
        #region Private fields

        /// <summary>
        /// The left offset.
        /// </summary>
        private double leftOffset;

        /// <summary>
        /// The top offset.
        /// </summary>
        private double topOffset;

        /// <summary>
        /// The child element.
        /// </summary>
        private UIElement childElement;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VisualBrushDragAdorner" /> class.
        /// </summary>
        /// <param name="owner">The owner.</param>
        /// <param name="child">The child.</param>
        /// <param name="opacity">The opacity.</param>
        public VisualBrushDragAdorner(UIElement owner, UIElement child, double opacity)
            : base(owner)
        {
            var size = GetRealSize(child);
            this.centerX = size.Width / 2;
            this.centerY = size.Height / 2;

            var visualBrush = new VisualBrush(child) { Opacity = opacity, AlignmentX = AlignmentX.Left, AlignmentY = AlignmentY.Top, Stretch = Stretch.None, };
            this.childElement = new Rectangle
                                    {
                                        RadiusX = 3,
                                        RadiusY = 3,
                                        Width = size.Width,
                                        Height = size.Height,
                                        Fill = visualBrush
                                    };
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        protected override int VisualChildrenCount => 1;

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Updates the position.
        /// </summary>
        /// <param name="point">The point.</param>
        public void UpdatePosition(Point point)
        {
            this.leftOffset = point.X;
            this.topOffset = point.Y;
            this.UpdatePosition();
        }

        /// <summary>
        /// Updates the left position.
        /// </summary>
        /// <param name="x">The x.</param>
        public void UpdateLeftPosition(double x)
        {
            this.leftOffset = x;
            this.UpdatePosition();
        }

        /// <summary>
        /// Updates the position.
        /// </summary>
        public void UpdatePosition()
        {
            var adorner = this.Parent as AdornerLayer;
            adorner?.Update(this.AdornedElement);
        }

        /// <summary>
        /// Gets the left position.
        /// </summary>
        /// <returns>The left position.</returns>
        public double GetLeftPosition()
        {
            return this.leftOffset;
        }

        /// <summary>
        /// Moves the left position by the specified amount.
        /// </summary>
        /// <param name="moveAmount">The move amount.</param>
        /// <returns>The actual move amount.</returns>
        public double MoveLeft(double moveAmount)
        {
            var position = this.GetLeftPosition();
            var newPosition = position + moveAmount;
            if (newPosition < 0)
            {
                newPosition = 0;
            }

            this.UpdateLeftPosition(newPosition);
            return this.GetLeftPosition() - position;
        }

        /// <summary>
        /// Returns a <see cref="T:System.Windows.Media.Transform" /> for the adorner, based on the transform that is currently applied to the adorned element.
        /// </summary>
        /// <param name="transform">The transform that is currently applied to the adorned element.</param>
        /// <returns>
        /// A transform to apply to the adorner.
        /// </returns>
        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            var result = new GeneralTransformGroup();
            result.Children.Add(new TranslateTransform(this.leftOffset, this.topOffset));

            var baseTransform = base.GetDesiredTransform(transform);
            if (baseTransform != null)
            {
                result.Children.Add(baseTransform);
            }

            return result;
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)" />, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>
        /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            if (0 != index)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return this.childElement;
        }

        /// <summary>
        /// When overridden in a derived class, measures the size in layout required for child elements and determines a size for the <see cref="T:System.Windows.FrameworkElement" />-derived class.
        /// </summary>
        /// <param name="availableSize">The available size that this element can give to child elements. Infinity can be specified as a value to indicate that the element will size to whatever content is available.</param>
        /// <returns>
        /// The size that this element determines it needs during layout, based on its calculations of child element sizes.
        /// </returns>
        protected override Size MeasureOverride(Size availableSize)
        {
            this.childElement.Measure(availableSize);
            return this.childElement.DesiredSize;
        }

        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement" /> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            this.childElement.Arrange(new Rect(this.childElement.DesiredSize));
            return finalSize;
        }

        /// <summary>
        /// Gets the real size of the child element.
        /// </summary>
        /// <param name="child">The child.</param>
        /// <returns>The real size.</returns>
        private static Size GetRealSize(UIElement child)
        {
            return child == null ? Size.Empty : child.RenderSize;
        }

        #endregion
    }
}
