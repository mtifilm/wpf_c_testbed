﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FrameworkElementAdorner.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>1/7/2016 3:38:11 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf.AdornedControl
{
    using System.Collections;
    using System.Windows;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// Class that defines a framework element adorner.
    /// Based on code from: http://www.codeproject.com/KB/WPF/WPFJoshSmith.aspx
    /// </summary>
    public class FrameworkElementAdorner : Adorner
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields
        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// The child.
        /// </summary>
        private readonly FrameworkElement child;

        /// <summary>
        /// The horizontal adorner placement.
        /// </summary>
        private readonly AdornerPlacement horizontalAdornerPlacement = AdornerPlacement.Inside;

        /// <summary>
        /// The vertical adorner placement.
        /// </summary>
        private readonly AdornerPlacement verticalAdornerPlacement = AdornerPlacement.Inside;

        /// <summary>
        /// The offset x.
        /// </summary>
        private readonly double offsetX;

        /// <summary>
        /// The offset y.
        /// </summary>
        private readonly double offsetY;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkElementAdorner"/> class.
        /// </summary>
        /// <param name="adornerChildElement">The adorner child element.</param>
        /// <param name="adornedElement">The adorned element.</param>
        public FrameworkElementAdorner(FrameworkElement adornerChildElement, FrameworkElement adornedElement)
            : base(adornedElement)
        {
            this.child = adornerChildElement;

            this.AddLogicalChild(adornerChildElement);
            this.AddVisualChild(adornerChildElement);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrameworkElementAdorner"/> class.
        /// </summary>
        /// <param name="adornerChildElement">The adorner child element.</param>
        /// <param name="adornedElement">The adorned element.</param>
        /// <param name="horizontalAdornerPlacement">The horizontal adorner placement.</param>
        /// <param name="verticalAdornerPlacement">The vertical adorner placement.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        public FrameworkElementAdorner(
            FrameworkElement adornerChildElement,
            FrameworkElement adornedElement,
            AdornerPlacement horizontalAdornerPlacement,
            AdornerPlacement verticalAdornerPlacement,
            double offsetX,
            double offsetY)
            : base(adornedElement)
        {
            this.child = adornerChildElement;
            this.horizontalAdornerPlacement = horizontalAdornerPlacement;
            this.verticalAdornerPlacement = verticalAdornerPlacement;
            this.offsetX = offsetX;
            this.offsetY = offsetY;

            adornedElement.SizeChanged += this.HandleAdornedElementSizeChanged;

            this.AddLogicalChild(adornerChildElement);
            this.AddVisualChild(adornerChildElement);
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets the position x.
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// Gets or sets the position y.
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// Gets the <see cref="T:System.Windows.UIElement" /> that this adorner is bound to.
        /// </summary>
        public new FrameworkElement AdornedElement
        {
            get
            {
                return (FrameworkElement)base.AdornedElement;
            }
        }

        /// <summary>
        /// Gets the number of visual child elements within this element.
        /// </summary>
        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// Gets an enumerator for logical child elements of this element.
        /// </summary>
        protected override IEnumerator LogicalChildren
        {
            get
            {
                var list = new ArrayList { this.child };
                return list.GetEnumerator();
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Disconnect the child element from the visual tree so that it may be reused later.
        /// </summary>
        public void DisconnectChild()
        {
            this.RemoveLogicalChild(this.child);
            this.RemoveVisualChild(this.child);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Implements any custom measuring behavior for the adorner.
        /// </summary>
        /// <param name="constraint">A size to constrain the adorner to.</param>
        /// <returns>
        /// A <see cref="T:System.Windows.Size" /> object representing the amount of layout space needed by the adorner.
        /// </returns>
        protected override Size MeasureOverride(Size constraint)
        {
            this.child.Measure(constraint);
            return this.child.DesiredSize;
        }

        /// <summary>
        /// When overridden in a derived class, positions child elements and determines a size for a <see cref="T:System.Windows.FrameworkElement" /> derived class.
        /// </summary>
        /// <param name="finalSize">The final area within the parent that this element should use to arrange itself and its children.</param>
        /// <returns>
        /// The actual size used.
        /// </returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            var x = this.PositionX;
            if (double.IsNaN(x))
            {
                x = this.DetermineX();
            }

            var y = this.PositionY;
            if (double.IsNaN(y))
            {
                y = this.DetermineY();
            }

            var adornerWidth = this.DetermineWidth();
            var adornerHeight = this.DetermineHeight();
            this.child.Arrange(new Rect(x, y, adornerWidth, adornerHeight));
            return finalSize;
        }

        /// <summary>
        /// Overrides <see cref="M:System.Windows.Media.Visual.GetVisualChild(System.Int32)" />, and returns a child at the specified index from a collection of child elements.
        /// </summary>
        /// <param name="index">The zero-based index of the requested child element in the collection.</param>
        /// <returns>
        /// The requested child element. This should not return null; if the provided index is out of range, an exception is thrown.
        /// </returns>
        protected override Visual GetVisualChild(int index)
        {
            return this.child;
        }

        /// <summary>
        /// Handles the adorned element size changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs"/> instance containing the event data.</param>
        private void HandleAdornedElementSizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.InvalidateMeasure();
        }

        /// <summary>
        /// Determines the x.
        /// </summary>
        /// <returns>The x position.</returns>
        private double DetermineX()
        {
            switch (this.child.HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    if (this.horizontalAdornerPlacement == AdornerPlacement.Outside)
                    {
                        return -this.child.DesiredSize.Width + this.offsetX;
                    }

                    return this.offsetX;

                case HorizontalAlignment.Right:
                    {
                        if (this.horizontalAdornerPlacement == AdornerPlacement.Outside)
                        {
                            var adornedWidth = this.AdornedElement.ActualWidth;
                            return adornedWidth + this.offsetX;
                        }
                        else
                        {
                            var adornerWidth = this.child.DesiredSize.Width;
                            var adornedWidth = this.AdornedElement.ActualWidth;
                            var x = adornedWidth - adornerWidth;
                            return x + this.offsetX;
                        }
                    }

                case HorizontalAlignment.Center:
                    {
                        var adornerWidth = this.child.DesiredSize.Width;
                        var adornedWidth = this.AdornedElement.ActualWidth;
                        var x = (adornedWidth / 2) - (adornerWidth / 2);
                        return x + this.offsetX;
                    }

                case HorizontalAlignment.Stretch:
                    return 0.0;
            }

            return 0.0;
        }

        /// <summary>
        /// Determines the y.
        /// </summary>
        /// <returns>The y position.</returns>
        private double DetermineY()
        {
            switch (this.child.VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    if (this.verticalAdornerPlacement == AdornerPlacement.Outside)
                    {
                        return -this.child.DesiredSize.Height + this.offsetY;
                    }

                    return this.offsetY;

                case VerticalAlignment.Bottom:
                    {
                        if (this.verticalAdornerPlacement == AdornerPlacement.Outside)
                        {
                            var adornedHeight = this.AdornedElement.ActualHeight;
                            return adornedHeight + this.offsetY;
                        }
                        else
                        {
                            var adornerHeight = this.child.DesiredSize.Height;
                            var adornedHeight = this.AdornedElement.ActualHeight;
                            var x = adornedHeight - adornerHeight;
                            return x + this.offsetY;
                        }
                    }

                case VerticalAlignment.Center:
                    {
                        var adornerHeight = this.child.DesiredSize.Height;
                        var adornedHeight = this.AdornedElement.ActualHeight;
                        var x = (adornedHeight / 2) - (adornerHeight / 2);
                        return x + this.offsetY;
                    }

                case VerticalAlignment.Stretch:
                    return 0.0;
            }

            return 0.0;
        }

        /// <summary>
        /// Determines the width.
        /// </summary>
        /// <returns>The width.</returns>
        private double DetermineWidth()
        {
            if (!double.IsNaN(this.PositionX))
            {
                return this.child.DesiredSize.Width;
            }

            switch (this.child.HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    return this.child.DesiredSize.Width;

                case HorizontalAlignment.Right:
                    return this.child.DesiredSize.Width;

                case HorizontalAlignment.Center:
                    return this.child.DesiredSize.Width;

                case HorizontalAlignment.Stretch:
                    return this.AdornedElement.ActualWidth;
            }

            return 0.0;
        }

        /// <summary>
        /// Determines the height.
        /// </summary>
        /// <returns>The height.</returns>
        private double DetermineHeight()
        {
            if (!double.IsNaN(this.PositionY))
            {
                return this.child.DesiredSize.Height;
            }

            switch (this.child.VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    return this.child.DesiredSize.Height;

                case VerticalAlignment.Bottom:
                    return this.child.DesiredSize.Height;

                case VerticalAlignment.Center:
                    return this.child.DesiredSize.Height;

                case VerticalAlignment.Stretch:
                    return this.AdornedElement.ActualHeight;
            }

            return 0.0;
        }

        #endregion
    }
}
