﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdornedControl.cs" company="MTI Film LLC">
// Copyright (c) 2016 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>1/7/2016 3:22:14 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf.AdornedControl
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Input;

    /// <summary>
    /// Class that defines an adorned control.
    /// </summary>
    public class AdornedControl : ContentControl
    {
        /////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The is adorner visible property.
        /// </summary>
        public static readonly DependencyProperty IsAdornerVisibleProperty = DependencyProperty.Register(
            "IsAdornerVisible",
            typeof(bool),
            typeof(AdornedControl),
            new FrameworkPropertyMetadata(IsAdornerVisiblePropertyChanged));

        /// <summary>
        /// The adorner content property.
        /// </summary>
        public static readonly DependencyProperty AdornerContentProperty = DependencyProperty.Register(
            "AdornerContent",
            typeof(FrameworkElement),
            typeof(AdornedControl),
            new FrameworkPropertyMetadata(AdornerContentPropertyChanged));

        /// <summary>
        /// The horizontal adorner placement property.
        /// </summary>
        public static readonly DependencyProperty HorizontalAdornerPlacementProperty = DependencyProperty.Register(
            "HorizontalAdornerPlacement",
            typeof(AdornerPlacement),
            typeof(AdornedControl),
            new FrameworkPropertyMetadata(AdornerPlacement.Inside));

        /// <summary>
        /// The vertical adorner placement property.
        /// </summary>
        public static readonly DependencyProperty VerticalAdornerPlacementProperty = DependencyProperty.Register(
            "VerticalAdornerPlacement",
            typeof(AdornerPlacement),
            typeof(AdornedControl),
            new FrameworkPropertyMetadata(AdornerPlacement.Inside));

        /// <summary>
        /// The adorner offset x property.
        /// </summary>
        public static readonly DependencyProperty AdornerOffsetXProperty = DependencyProperty.Register("AdornerOffsetX", typeof(double), typeof(AdornedControl));

        /// <summary>
        /// The adorner offset y property.
        /// </summary>
        public static readonly DependencyProperty AdornerOffsetYProperty = DependencyProperty.Register("AdornerOffsetY", typeof(double), typeof(AdornedControl));

        /// <summary>
        /// The show adorner command.
        /// </summary>
        public static readonly RoutedCommand ShowAdornerCommand = new RoutedCommand("ShowAdorner", typeof(AdornedControl));

        /// <summary>
        /// The hide adorner command.
        /// </summary>
        public static readonly RoutedCommand HideAdornerCommand = new RoutedCommand("HideAdorner", typeof(AdornedControl));

        /// <summary>
        /// The show adorner command binding.
        /// </summary>
        private static readonly CommandBinding ShowAdornerCommandBinding = new CommandBinding(ShowAdornerCommand, OnShowAdornerCommandExecuted);

        /// <summary>
        /// The hide adorner command binding.
        /// </summary>
        private static readonly CommandBinding HideAdornerCommandBinding = new CommandBinding(HideAdornerCommand, OnHideAdornerCommandExecuted);

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        /// <summary>
        /// Caches the adorner layer.
        /// </summary>
        private AdornerLayer adornerLayer;

        /// <summary>
        /// The actual adorner create to contain our 'adorner UI content'.
        /// </summary>
        private FrameworkElementAdorner adorner = null;

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes static members of the <see cref="AdornedControl"/> class.
        /// </summary>
        static AdornedControl()
        {
            CommandManager.RegisterClassCommandBinding(typeof(AdornedControl), ShowAdornerCommandBinding);
            CommandManager.RegisterClassCommandBinding(typeof(AdornedControl), HideAdornerCommandBinding);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdornedControl"/> class.
        /// </summary>
        public AdornedControl()
        {
            this.DataContextChanged += this.HandleAdornedControlDataContextChanged;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this instance is adorner visible.
        /// </summary>
        public bool IsAdornerVisible
        {
            get
            {
                return (bool)this.GetValue(IsAdornerVisibleProperty);
            }

            set
            {
                this.SetValue(IsAdornerVisibleProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the content of the adorner.
        /// </summary>
        public FrameworkElement AdornerContent
        {
            get
            {
                return (FrameworkElement)this.GetValue(AdornerContentProperty);
            }

            set
            {
                this.SetValue(AdornerContentProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the horizontal adorner placement.
        /// </summary>
        public AdornerPlacement HorizontalAdornerPlacement
        {
            get
            {
                return (AdornerPlacement)this.GetValue(HorizontalAdornerPlacementProperty);
            }

            set
            {
                this.SetValue(HorizontalAdornerPlacementProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the vertical adorner placement.
        /// </summary>
        public AdornerPlacement VerticalAdornerPlacement
        {
            get
            {
                return (AdornerPlacement)this.GetValue(VerticalAdornerPlacementProperty);
            }

            set
            {
                this.SetValue(VerticalAdornerPlacementProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the adorner offset x.
        /// </summary>
        public double AdornerOffsetX
        {
            get
            {
                return (double)this.GetValue(AdornerOffsetXProperty);
            }

            set
            {
                this.SetValue(AdornerOffsetXProperty, value);
            }
        }

        /// <summary>
        /// Gets or sets the adorner offset y.
        /// </summary>
        public double AdornerOffsetY
        {
            get
            {
                return (double)this.GetValue(AdornerOffsetYProperty);
            }

            set
            {
                this.SetValue(AdornerOffsetYProperty, value);
            }
        }

        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods

        /// <summary>
        /// Show the adorner.
        /// </summary>
        public void ShowAdorner()
        {
            this.IsAdornerVisible = true;
        }

        /// <summary>
        /// Hide the adorner.
        /// </summary>
        public void HideAdorner()
        {
            this.IsAdornerVisible = false;
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate" />.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.ShowOrHideAdornerInternal();
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// Handles the is adorner visible property changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void IsAdornerVisiblePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var c = (AdornedControl)d;
            c.ShowOrHideAdornerInternal();
        }

        /// <summary>
        /// Handles the adorner content property changed.
        /// </summary>
        /// <param name="d">The d.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AdornerContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var c = (AdornedControl)d;
            c.ShowOrHideAdornerInternal();
        }

        /// <summary>
        /// Called when the show adorner command is executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private static void OnShowAdornerCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var c = (AdornedControl)sender;
            c.ShowAdorner();
        }

        /// <summary>
        /// Called when the hide adorner command is executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private static void OnHideAdornerCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var c = (AdornedControl)sender;
            c.HideAdorner();
        }

        /// <summary>
        /// Handles the adorned control data context changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleAdornedControlDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.UpdateAdornerDataContext();
        }

        /// <summary>
        /// Updates the adorner data context.
        /// </summary>
        private void UpdateAdornerDataContext()
        {
            if (this.AdornerContent != null)
            {
                this.AdornerContent.DataContext = this.DataContext;
            }
        }

        /// <summary>
        /// Shows the or hide adorner internal.
        /// </summary>
        private void ShowOrHideAdornerInternal()
        {
            if (this.IsAdornerVisible)
            {
                this.ShowAdornerInternal();
            }
            else
            {
                this.HideAdornerInternal();
            }
        }

        /// <summary>
        /// Shows the adorner internal.
        /// </summary>
        private void ShowAdornerInternal()
        {
            if (this.adorner != null)
            {
                // Already adorned.
                return;
            }

            if (this.AdornerContent == null)
            {
                return;
            }

            if (this.adornerLayer == null)
            {
                this.adornerLayer = AdornerLayer.GetAdornerLayer(this);
            }

            if (this.adornerLayer == null)
            {
                return;
            }

            this.adorner = new FrameworkElementAdorner(
                this.AdornerContent,
                this,
                this.HorizontalAdornerPlacement,
                this.VerticalAdornerPlacement,
                this.AdornerOffsetX,
                this.AdornerOffsetY);
            this.adornerLayer.Add(this.adorner);
            this.UpdateAdornerDataContext();
        }

        /// <summary>
        /// Hides the adorner internal.
        /// </summary>
        private void HideAdornerInternal()
        {
            if (this.adornerLayer == null || this.adorner == null)
            {
                // Not already adorned.
                return;
            }

            this.adornerLayer.Remove(this.adorner);
            this.adorner.DisconnectChild();

            this.adorner = null;
            this.adornerLayer = null;
        }

        #endregion
    }
}
