﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SortGlyphAdorner.cs" company="MTI Film LLC">
// Copyright (c) 2013 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>MTI\peter.firth</author>
// <date>12/19/2013 2:37:43 PM</date>
// <summary></summary>
// --------------------------------------------------------------------------------------------------------------------

namespace MtiFilm.Control.Wpf
{
    using System.ComponentModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;

    /// <summary>
    /// Class that defines the sort glyph adorner.
    /// </summary>
    public class SortGlyphAdorner : Adorner
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////
        #region Const and readonly fields

        /// <summary>
        /// The grid view column header
        /// </summary>
        private readonly GridViewColumnHeader gridViewColumnHeader;

        /// <summary>
        /// The direction
        /// </summary>
        private readonly ListSortDirection direction;

        /// <summary>
        /// The sort glyph
        /// </summary>
        private readonly ImageSource sortGlyph;

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    
        #region Private fields

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SortGlyphAdorner" /> class.
        /// </summary>
        /// <param name="columnHeader">The column header.</param>
        /// <param name="sortDirection">The sort direction.</param>
        /// <param name="glyph">The glyph.</param>
        public SortGlyphAdorner(GridViewColumnHeader columnHeader, ListSortDirection sortDirection, ImageSource glyph)
            : base(columnHeader)
        {
            this.gridViewColumnHeader = columnHeader;
            this.direction = sortDirection;
            this.sortGlyph = glyph;
        }

        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////
        #region Public methods
        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////
        #region Private methods

        /// <summary>
        /// When overridden in a derived class, participates in rendering operations that are directed by the layout system. The rendering instructions for this element are not used directly when this method is invoked, and are instead preserved for later asynchronous use by layout and drawing.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions for a specific element. This context is provided to the layout system.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            if (this.sortGlyph != null)
            {
                var x = this.gridViewColumnHeader.ActualWidth - 13;
                var y = (this.gridViewColumnHeader.ActualHeight / 2) - 5;
                var rect = new Rect(x, y, 10, 10);
                drawingContext.DrawImage(this.sortGlyph, rect);
            }
            else
            {
                drawingContext.DrawGeometry(Brushes.LightGray, new Pen(Brushes.Gray, 1.0), this.GetDefaultGlyph());
            }
        }

        /// <summary>
        /// Gets the default glyph.
        /// </summary>
        /// <returns>The default glyph.</returns>
        private Geometry GetDefaultGlyph()
        {
            var x1 = this.gridViewColumnHeader.ActualWidth - 13;
            var x2 = x1 + 10;
            var x3 = x1 + 5;
            var y1 = (this.gridViewColumnHeader.ActualHeight / 2) - 3;
            var y2 = y1 + 5;

            if (this.direction == ListSortDirection.Ascending)
            {
                var tmp = y1;
                y1 = y2;
                y2 = tmp;
            }

            var pathSegmentCollection = new PathSegmentCollection { new LineSegment(new Point(x2, y1), true), new LineSegment(new Point(x3, y2), true) };

            var pathFigure = new PathFigure(
                new Point(x1, y1),
                pathSegmentCollection,
                true);

            var pathFigureCollection = new PathFigureCollection { pathFigure };

            var pathGeometry = new PathGeometry(pathFigureCollection);
            return pathGeometry;
        }

        #endregion
    }
}
