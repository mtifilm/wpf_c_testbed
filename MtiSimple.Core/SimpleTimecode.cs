﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MtiSimple.Core
{
    // A simple timecode has two properties
    // The frame number and precision
    // This object is IMMUTABLE
    public class SimpleTimecode
    {
        public SimpleTimecode()
        {
        }

        public SimpleTimecode(String timecodeString)
        {
            this.Frame = Int32.Parse(timecodeString);
            this.Precision = timecodeString.Length;
        }

        public SimpleTimecode(String timecodeString, int precision)
        {
            this.Frame = Int32.Parse(timecodeString);
            this.Precision = precision;
        }

        public SimpleTimecode(int frame, int precision)
        {
            this.Frame = frame;
            this.Precision = precision;
        }

        public SimpleTimecode AddFrames(int frames)
        {
            return new SimpleTimecode(this.Frame + frames, this.Precision);
        }

        [JsonProperty]
        public int Precision { get; private set; }

        [JsonProperty]
        public int Frame { get; private set; }

        public override string ToString()
        {
            return Frame.ToString("D" + Precision.ToString());
        }
    }
}
