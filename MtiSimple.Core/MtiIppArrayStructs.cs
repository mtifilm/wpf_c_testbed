﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using MtiSimple.Core.Annotations;

namespace MtiSimple.Core
{
    // These really need to be defined in a common place but for now just keep in model
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MtiPoint<T>
    {
        public T X;
        public T Y;

        public MtiPoint(T x, T y)
        {
            X = x;
            Y = y;
        }
        public override string ToString()
        {
            return "(" + this.X.ToString() + " , " + this.Y.ToString() + ")";
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MtiSize<T>
    {
        public T Width;
        public T Height;
        public int Depth;

        public MtiSize(T width, T height, int depth = 1)
        {
            this.Width = width;
            this.Height = height;
            this.Depth = depth;
        }

        public override string ToString()
        {
            string imageType = "unknown";
            switch (this.Depth)
            {
                case 1:
                    imageType = "Mono";
                    break;

                case 3:
                    imageType = "RGB";
                    break;

                case 4:
                    imageType = "RGBA";
                    break;
            }

            return "(" + this.Width.ToString() + " x " + this.Height.ToString() + ") " + imageType;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MtiRectInt
    {
        public int X;
        public int Y;
        public int Width;
        public int Height;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct MtiRect<T> : INotifyPropertyChanged, IEquatable<MtiRect<T>>
    {
        private T _x;
        private T _y;
        private T _width;
        private T _height;

        public T X
        {
            get => _x;
            set => _x = value;
        }

        public T Y
        {
            get => _y;
            set => _y = value;
        }

        public T Width
        {
            get => _width;
            set => _width = value;
        }
        public T Height
        {
            get => _height;
            set => _height = value;
        }

        public MtiSize<T> RectSize => new MtiSize<T>(this.Width, this.Height);

        public MtiPoint<T> TopLeft() => new MtiPoint<T>(X, Y);
        public MtiPoint<T> TopRight()
        {
            dynamic xd = X;
            dynamic wd = Width;
            return new MtiPoint<T>(xd + wd, Y);
        }

        public MtiPoint<T> BottomLeft()
        {
            dynamic yd = Y;
            dynamic hd = Height;
            return new MtiPoint<T>(X, yd + hd);
        }

        public MtiPoint<T> BottomRight()
        {
            dynamic xd = X;
            dynamic wd = Width;
            dynamic yd = Y;
            dynamic hd = Height;
            return new MtiPoint<T>(xd + wd, yd + hd);
        }

        public override string ToString()
        {
            return "(" + this.X.ToString() + ", " + this.Y.ToString() + ", " + this.Width.ToString() + ". " + this.Height.ToString() + ")";
        }

        public static bool operator ==(MtiRect<T> obj1, MtiRect<T> obj2)
        {
            return obj1.Equals(obj2);
        }

        // this is second one '!='
        public static bool operator !=(MtiRect<T> obj1, MtiRect<T> obj2)
        {
            return !obj1.Equals(obj2);
        }

        public bool Equals(MtiRect<T> other)
        {
            return EqualityComparer<T>.Default.Equals(X, other.X) && EqualityComparer<T>.Default.Equals(Y, other.Y) && EqualityComparer<T>.Default.Equals(Width, other.Width) && EqualityComparer<T>.Default.Equals(Height, other.Height);
        }

        public override bool Equals(object obj)
        {
            return obj is MtiRect<T> other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EqualityComparer<T>.Default.GetHashCode(X);
                hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Y);
                hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Width);
                hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Height);
                return hashCode;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class MtiRoi<T> : BindableObject where T : new()
    {
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return base.Equals((MtiRoi<T>) obj);
        }

        public override int GetHashCode()
        {
            var hashCode = EqualityComparer<T>.Default.GetHashCode(X);
            hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Y);
            hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Width);
            hashCode = (hashCode * 397) ^ EqualityComparer<T>.Default.GetHashCode(Height);
            return hashCode;
        }

        public T X
        {
            get => this.GetCurrentValue<T>();
            set => this.SetAndRaiseOnChange(value);
        }

        public T Y
        {
            get => this.GetCurrentValue<T>();
            set => this.SetAndRaiseOnChange(value);
        }
        public T Width
        {
            get => this.GetCurrentValue<T>();
            set => this.SetAndRaiseOnChange(value);
        }

        public T Height
        {
            get => this.GetCurrentValue<T>();
            set => this.SetAndRaiseOnChange(value);
        }

        public override string ToString()
        {
            return "(" + this.X.ToString() + ", " + this.Y.ToString() + ", " + this.Width.ToString() + ". " + this.Height.ToString() + ")";
        }

        public static bool operator ==(MtiRoi<T> obj1, MtiRoi<T> obj2)
        {
            return obj1?.Equals(obj2) == true;
        }

        // this is second one '!='
        public static bool operator !=(MtiRoi<T> obj1, MtiRoi<T> obj2)
        {
            return obj1?.Equals(obj2) == false;
        }

        public MtiPoint<T> TopLeft() => new MtiPoint<T>(X, Y);
        public MtiPoint<T> TopRight()
        {
            dynamic xd = X;
            dynamic wd = Width;
            return new MtiPoint<T>(xd + wd, Y);
        }

        public MtiPoint<T> BottomLeft()
        {
            dynamic yd = Y;
            dynamic hd = Height;
            return new MtiPoint<T>(X, yd + hd);
        }

        public MtiPoint<T> BottomRight()
        {
            dynamic xd = X;
            dynamic wd = Width;
            dynamic yd = Y;
            dynamic hd = Height;
            return new MtiPoint<T>(xd + wd, yd + hd);
        }
    }
}
