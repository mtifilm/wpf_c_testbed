﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MtiSimple.Core
{
    public static class MtiMath
    {
        /// <summary>
        ///  Usual clamp
        /// TODO: handle null values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="max"></param>
        /// <param name="min"></param>
        /// <returns></returns>
        public static T Clamp<T>(T value, T min, T max) where T : System.IComparable<T>
        {
            T result = (value.CompareTo(max) > 0) ? max : value;
            return (result.CompareTo(min) < 0) ? min : result;
        }
    }
}
