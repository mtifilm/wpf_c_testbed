﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FairSemaphore.cs" company="MTI Film LLC">
// Copyright (c) 2012 by MTI Film, LLC. All Rights Reserved
// </copyright>
// <author>Wasabe-XL\mbraca</author>
// <date>3/31/2012 8:27:10 AM</date>
// <summary>
// This is a simple semaphore that guarantees FIFO releasing of waiters for fairness.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

////#define USE_SEMAPHORE_SLIM

namespace MtiSimple.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    /// <summary>
    /// This is a simple mutex that guarantees FIFO releasing of waiters for fairness.
    /// </summary>
    public class FairSemaphore
    {
        //////////////////////////////////////////////////
        // Const and readonly fields
        //////////////////////////////////////////////////

        #region Const and readonly fields

        /// <summary>
        /// The FIFO representing the order that threads arrived in. Each thread gets its own event to wait on
        ///  It's  a list because sometimes we need to have new entries jump the queue.
        /// </summary>
        private readonly List<QueueEntry> queue = new List<QueueEntry>();

        /// <summary>
        /// THe free list to recycle wait event notifiers..
        /// </summary>
        private readonly Queue<ManualResetEventSlim> recycler = new Queue<ManualResetEventSlim>(); 

        /// <summary>
        /// Mutex to control access to the queue.
        /// </summary>
        private readonly SemaphoreSlim mutex = new SemaphoreSlim(1, 1);

        #endregion

        //////////////////////////////////////////////////
        // Private fields, all static before non-static
        //////////////////////////////////////////////////    

        #region Private fields

        #endregion

        //////////////////////////////////////////////////
        // Constructors
        //////////////////////////////////////////////////

        #region Constructors
        #endregion

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////

        #region Properties
        #endregion

        //////////////////////////////////////////////////
        // Public methods, all static before non-static
        //////////////////////////////////////////////////

        #region Public methods

        /// <summary>
        /// Acquires access to the semaphore-protected resource.
        /// </summary>
        public void Acquire()
        {
            // This is a struct so is allocated on the stack.
            var myQueueEntry = new QueueEntry();

            this.mutex.Wait();
            try
            {
                myQueueEntry.ThreadId = Thread.CurrentThread.ManagedThreadId;

                // Fill in our queue
                if (this.recycler.Count == 0)
                {
                    myQueueEntry.WaitEvent = new ManualResetEventSlim(false);
                }
                else
                {
                    myQueueEntry.WaitEvent = this.recycler.Dequeue();
                    myQueueEntry.WaitEvent.Reset();
                }

                // We get to continue if nothing's going on.
                if (this.queue.Count == 0)
                {
                    // We're good to go... the semaphore will only be used if we re-enter
                    // this code from the same thread (either a nested call or we might
                    // be processing a message on this (usually the main) thread while
                    // in a wait).
                    this.queue.Add(myQueueEntry);
                    return;
                }

                // We get to continue if we already "own" the semaphore. The owner will
                // always be the thread whose entry is first in the queue.
                var ownerId = this.queue.First().ThreadId;
                if (myQueueEntry.ThreadId == ownerId)
                {
                    // We're good to go... need to put our semaphore at the head of the queue.
                    this.queue.Insert(0, myQueueEntry);
                    return;
                }

                // We need to wait our turn. Insert our entry at the end of the queue,
                // unless this thread is already in the queue in which case we need to
                // insert this entry BEFORE that one to avoid certain deadlock!
                int index = this.queue.TakeWhile(entryInQueue => entryInQueue.ThreadId != myQueueEntry.ThreadId).Count();
                this.queue.Insert(index, myQueueEntry);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            {
            }
            // ReSharper restore EmptyGeneralCatchClause
            finally
            {
                this.mutex.Release();
            }

            // Wait for the resource to become available.
            // ReSharper disable PossibleNullReferenceException
            myQueueEntry.WaitEvent.Wait();

            // ReSharper restore PossibleNullReferenceException
        }

        /// <summary>
        /// Releases the next waiter in the queue.
        /// </summary>
        public void Release()
        {
            this.mutex.Wait();
            try
            {
                // Recycle our wait event.
                this.recycler.Enqueue(this.queue.First().WaitEvent);

                // Remove our entry from the queue.
                this.queue.RemoveAt(0);

                // Signal the next waiter in the queue, if any.
                if (this.queue.Count > 0)
                {
                    this.queue.First().WaitEvent.Set();
                }
            }           
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            {
            }
            // ReSharper restore EmptyGeneralCatchClause
            finally
            {
                this.mutex.Release();
            }
        }

        /// <summary>
        /// Gets auto-locking object that controls this semaphore instance.
        /// </summary>
        /// <returns>The auto locker.</returns>
        public AutoLocker GetAutoLocker()
        {
            return new AutoLocker(this);
        }

        #endregion

        //////////////////////////////////////////////////
        // Private methods, all static before non-static
        //////////////////////////////////////////////////

        #region Private methods
        #endregion

        /////////////////////////////////
        // Embedded structs and classes
        /////////////////////////////////

        #region Embedded structs

        /// <summary>
        /// Queue entries consist of the thread ID and the semaphore
        /// </summary>
        private struct QueueEntry
        {
            /// <summary>
            /// Gets or sets the thread ID, used to prevent deadlock if the same thread  locks the
            /// semaphore twice, in which case we need to break  the fairness semantics.
            /// </summary>
            /// <value>
            /// The thread id.
            /// </value>
            public int ThreadId;

            /// <summary>
            /// Gets or sets the individual semaphores.
            /// </summary>
            /// <value>
            /// The semaphore.
            /// </value>
            public ManualResetEventSlim WaitEvent;
        }

        /// <summary>
        /// An auto-locker class that uses fair semaphores.
        /// Intended only for use in a using statement.
        /// </summary>
        public class AutoLocker : IDisposable
        {
            /// <summary>
            /// My semaphore.
            /// </summary>
            private readonly FairSemaphore mySemaphore;

            /// <summary>
            /// Initializes a new instance of the <see cref="AutoLocker"/> class.
            /// </summary>
            /// <param name="semaphore">The semaphore to auto-lock.</param>
            public AutoLocker(FairSemaphore semaphore)
            {
                this.mySemaphore = semaphore;
                semaphore.Acquire();
            }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            public void Dispose()
            {
                this.mySemaphore.Release();
            }
        }

        #endregion
    }
}
