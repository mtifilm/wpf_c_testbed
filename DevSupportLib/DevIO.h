#pragma once
#include <vector>
#include <sstream>
#include <iostream>
#include <string>
#include "Ippheaders.h"
#include "Dpx/DPXHeader.h"

namespace DevIO
{
   void *getPinnedMemory(size_t bytes);

	std::pair<Ipp16uArray, int> readDpxFile(const string &fileName, bool normalize = true, int imageIndex=0);
	dpx::Header readDpxHeader(const string & fileName);
	
    // TBD: Interface needs a template, have to figure that out.
    void writeDpxFile(const string &fileName, const Ipp16uArray &dataArray, const string &outFileName);
}

