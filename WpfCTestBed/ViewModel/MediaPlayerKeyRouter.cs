﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MtiSimple.Core;
using WpfCTestBed.View;

namespace WpfCTestBed.ViewModel
{
    static class MediaPlayerKeyRouter
    {

        //////////////////////////////////////////////////
        // Public Properties, all static before non-static
        //////////////////////////////////////////////////
        #region Properties

        /// <summary>
        /// Called when the active media player is changed
        /// </summary>
        /// <param name="newActiveMediaPlayer">The new active media player.</param>
        public delegate void ActiveMediaPlayerChangedEventHandler(SimplePlayerControl newActiveMediaPlayer);

        /// <summary>
        /// Occurs when the active media player is changed.
        /// </summary>
        public static event ActiveMediaPlayerChangedEventHandler ActiveMediaPlayerChanged;

        /// <summary>
        /// Gets the active media player.
        /// </summary>
        public static SimplePlayerControl ActiveMediaPlayer { get; set; }

        #endregion
        public static void SetKeyBindings(InputBindingCollection inputBindings)
        {
            inputBindings.Add(new KeyBinding(SimplePlayerCommands.JogBackwardCommand,
                new NakedKeyGesture(Key.S)));

            inputBindings.Add(new KeyBinding(SimplePlayerCommands.JogForwardCommand,
                new NakedKeyGesture(Key.F)));

            inputBindings.Add(new KeyBinding(SimplePlayerCommands.MarkInCommand,
                new NakedKeyGesture(Key.E)));

            inputBindings.Add(new KeyBinding(SimplePlayerCommands.MarkOutCommand,
                new NakedKeyGesture(Key.R)));

            inputBindings.Add(new KeyBinding(SimplePlayerCommands.MarkEntireClipCommand,
                new NakedKeyGesture(Key.H)));

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.GotoMarkInCommand,
                Key = Key.E,
                Modifiers = ModifierKeys.Shift
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.GotoMarkOutCommand,
                Key = Key.R,
                Modifiers = ModifierKeys.Shift
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.PlayForwardCommand,
                Key = Key.V
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.PlayReverseCommand,
                Key = Key.X
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.PlayForwardLoopCommand,
                Key = Key.V,
                Modifiers = ModifierKeys.Shift
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.PlayReverseLoopCommand,
                Key = Key.X,
                Modifiers = ModifierKeys.Shift
            });

            inputBindings.Add(new KeyBinding()
            {
                Command = SimplePlayerCommands.StopCommand,
                Key = Key.Space
            });
        }

        public static void SetCommandBindings(CommandBindingCollection commandBindings)
        {
            commandBindings.Add(new CommandBinding(SimplePlayerCommands.JogForwardCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.JogBackwardCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.MarkOutCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.MarkInCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.GotoMarkOutCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.GotoMarkInCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.MarkEntireClipCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.PlayForwardCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.PlayReverseCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.PlayForwardLoopCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.PlayReverseLoopCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.StopCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.RestoreOriginalSizeCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.StartImageGrabCommand, OnCommandExecuted,
                CanExecuteCommand));

            commandBindings.Add(new CommandBinding(SimplePlayerCommands.EndImageGrabCommand, OnCommandExecuted,
                CanExecuteCommand));
        }

        /// <summary>
        /// Called when a media player command is executed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private static void OnCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var command = e.Command;
            // Here we translate commands with keystrokes
            // There must be a better way as this means the commands are not queue
            var isShiftDown = Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift);
            var isAltDown = Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt);
            var isCtrlDown = Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl);

            var pureShiftDown = isShiftDown && !isAltDown && !isCtrlDown;
            if (pureShiftDown)
            {
                if (command == SimplePlayerCommands.PlayForwardCommand)
                {
                    command = SimplePlayerCommands.PlayForwardLoopCommand;
                }
                else if (command == SimplePlayerCommands.PlayReverseCommand)
                {
                    command = SimplePlayerCommands.PlayReverseLoopCommand;
                }
                else if (command == SimplePlayerCommands.MarkInCommand)
                {
                    command = SimplePlayerCommands.GotoMarkInCommand;
                }
                else if (command == SimplePlayerCommands.MarkOutCommand)
                {
                    command = SimplePlayerCommands.GotoMarkOutCommand;
                }
            }

            ActiveMediaPlayer?.SimplePlayer.HandleMediaPlayerCommand(command);
        }

        /// <summary>
        /// Determines whether this instance can execute number pad digit command - basically we return true if there is a receiver for the events..
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.CanExecuteRoutedEventArgs"/> instance containing the event data.</param>
        private static void CanExecuteCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ActiveMediaPlayer?.Source != null;
        }
    }
}
