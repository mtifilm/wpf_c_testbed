﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MtiSimple.Core;
using Newtonsoft.Json;
using TestBed.ManagedApi;
using WpfCTestBed.Annotations;
using WpfCTestBed.Model;
using WpfCTestBed.View;
using WpfCTestBed.ViewModel;

namespace WpfCTestBed.TestBedViewModel
{
    // Note, this is really a View and a View model
    // However I screwed up originally and don't have time to fix it
    public class TestBedViewModel : UserControl, INotifyPropertyChanged
    {
        // TODO: this is somewhat inside out, create SimplePlayerControl and set gui.
        public TestBedViewModel(SimplePlayerControl simplePlayerControl)
        {
            this.SimplePlayerControl = simplePlayerControl;
            this.LoadSettings();

            this.TestBedModel.PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(TestBedModel.PreviewMode):
                    SimplePlayerControl.SimplePlayer.Redraw();
                    break;

                case nameof(TestBedModel.LastProcessedTimecode):
                    SimplePlayerControl.SimplePlayer.CurrentTimecode = TestBedModel.LastProcessedTimecode;
                    break;
            }
        }

        public void CanExecuteCommand(object sender, CanExecuteRoutedEventArgs e)
        {
            if (e.Command == ToolProcessCommands.PreprocessStartCommand)
            {
                e.CanExecute = TestBedModel.ProcessingStatus != ProcessingStatusEnum.Processing;
            }
            else if (e.Command == ToolProcessCommands.PreprocessCancelCommand)
            {
                e.CanExecute = TestBedModel.ProcessingStatus == ProcessingStatusEnum.Processing;
            }
        }

        public void OnCommandExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command == ToolProcessCommands.PreprocessStartCommand)
            {
                // Collect data, this is unstable but I don't care
                this.TestBedModel.ProcessRoi = this.SimplePlayerControl.ProcessRoi;
                this.TestBedModel.DispatchCommands(ToolProcessingCommandEnum.StartPreprocess);
            }
            if (e.Command == ToolProcessCommands.PreprocessCancelCommand)
            {
                // Collect data, this is unstable but I don't care
                this.TestBedModel.ProcessRoi = this.SimplePlayerControl.ProcessRoi;
                this.TestBedModel.DispatchCommands(ToolProcessingCommandEnum.CancelPreprocess);
            }

            if (e.Command == ToolProcessCommands.ProcessStartCommand)
            {
                // Collect data, this is unstable but I don't care
                this.TestBedModel.PreviewMode = true;
                this.TestBedModel.ProcessRoi = this.SimplePlayerControl.ProcessRoi;
                this.TestBedModel.DispatchCommands(ToolProcessingCommandEnum.StartProcess);
            }

            if (e.Command == ToolProcessCommands.ProcessCancelCommand)
            {
                // Collect data, this is unstable but I don't care
                this.TestBedModel.ProcessRoi = this.SimplePlayerControl.ProcessRoi;
                this.TestBedModel.DispatchCommands(ToolProcessingCommandEnum.CancelProcess);
            }
        }

        public TestBedModel TestBedModel { get; set; } = new TestBedModel();

        public SimplePlayerControl SimplePlayerControl { get; private set; }

        public void LoadSettings()
        {
            if (File.Exists(TestBedClipFile))
            {
                var simpleClipsSerializer = System.IO.File.ReadAllText(TestBedClipFile);
                if (simpleClipsSerializer != String.Empty)
                {
                    this.SimpleClips =
                        JsonConvert.DeserializeObject<ObservableCollection<SimpleClip>>(simpleClipsSerializer);
                }
            }

            var startClipName = Properties.Settings.Default.StartClipName;
            this.SelectedClip = SimpleClips.FirstOrDefault(s => String.Equals(s.ClipName, startClipName, StringComparison.CurrentCultureIgnoreCase));
        }

        public void SaveSettings()
        {
            //Properties.Settings.Default.SimpleClips = JsonConvert.SerializeObject(SimpleClips);
            System.IO.File.WriteAllText(TestBedClipFile, JsonConvert.SerializeObject(SimpleClips));

            Properties.Settings.Default.StartClipName = String.IsNullOrWhiteSpace(this.SelectedClip?.ClipName) ? String.Empty : this.SelectedClip?.ClipName;
            SimplePlayerControl.SaveSettings();
            Properties.Settings.Default.Save();
        }

        private static string TestBedClipFile =>
            System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                "TestBed.json");

        public ObservableCollection<SimpleClip> SimpleClips { get; set; } = new ObservableCollection<SimpleClip>();

        /// <summary>
        /// SelectedClip dependency processing
        /// </summary>
        public static readonly DependencyProperty SelectedClipProperty =
            DependencyProperty.Register("SelectedClip",
                typeof(SimpleClip),
                typeof(TestBedViewModel),
                new PropertyMetadata(null, new PropertyChangedCallback(OnSelectedClipChanged)));

        protected static void OnSelectedClipChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is TestBedViewModel viewModel))
            {
                return;
            }

            if (viewModel.SimplePlayerControl == null)
            {
                return;
            }

            var clip = e.NewValue as SimpleClip;
            NativeMethodsTestBed.InitCache();
            viewModel.TestBedModel.PreviewMode = false;
            viewModel.SimplePlayerControl.Source = clip;
            viewModel.TestBedModel.SourceClip = clip;
        }

        public SimpleClip SelectedClip
        {
            get => (SimpleClip) GetValue(SelectedClipProperty);
            set => SetValue(SelectedClipProperty, value);
        }

        /// <summary>
        /// Current MediaPlayerControl processing
        /// </summary>
        public static readonly DependencyProperty MediaPlayerControlProperty =
            DependencyProperty.Register("MediaPlayerControl",
                typeof(SimplePlayerControl),
                typeof(TestBedViewModel),
                new PropertyMetadata(null, new PropertyChangedCallback(OnMediaPlayerControlControlChanged)));


        protected static void OnMediaPlayerControlControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }

        public SimplePlayerControl MediaPlayerControl
        {
            get => (SimplePlayerControl)GetValue(MediaPlayerControlProperty);
            set => SetValue(MediaPlayerControlProperty, value);
        }

        public void ReplaceSimpleClip(SimpleClip oldClip, SimpleClip newClip)
        {
            var clip = this.SimpleClips.FirstOrDefault(c => String.Equals(c.ClipName, oldClip?.ClipName, StringComparison.CurrentCultureIgnoreCase));
            var index = this.SimpleClips.IndexOf(clip);
            if (index >= 0)
            {
                this.SimpleClips[index] = newClip;
            }
        }

        public void DeleteClipName(string clipName)
        {
            var clip = this.SimpleClips.FirstOrDefault(c => String.Equals(c.ClipName, clipName, StringComparison.CurrentCultureIgnoreCase));
            if (SimplePlayerControl.Source.ClipName == clip?.ClipName)
            {
                SimplePlayerControl.Source = null;
            }

            this.SimpleClips.Remove(clip);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
