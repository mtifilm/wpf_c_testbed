﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Threading;
using MtiSimple.Core;
using TestBed.ManagedApi;
using WpfCTestBed.ViewModel;

namespace WpfCTestBed.Model
{
    // This is wrong, stupid duplication of code
    // TODO: make only one processor.
    public class TestBedModel : BindableObject
    {
        public TestBedModel()
        {
            this.ProcessingClip = new SimpleClip();
            this.InitializeBackgroundWorkers();
            this.FramesPreprocessed = 0;
            this.FramesProcessed = 0;
            this.PropertyChanged += TestBedModelPropertyChanged;
            this.ProcessedSubfolder = "(V1)";
            this.Passes = 1;
        }

        private void TestBedModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "MarkedFrames":
                case nameof(ProcessingClip):
                case nameof(ProcessingStatus):
                    this.ReadyToPreprocess = (this.ProcessingStatus != ProcessingStatusEnum.Processing) &&
                                             (ProcessingClip?.MarksAreValid() == true);
                    if (ProcessingClip == null)
                    {
                        this.PreprocessErrorMessage = "No clip loaded";
                    }
                    else
                    {
                        if (ProcessingClip.MarksAreValid() == false)
                        {
                            this.PreprocessErrorMessage = "Invalid Marks";
                        }
                    }

                    break;

                case nameof(PreviewMode):
                    NativeMethodsTestBed.SetPreviewMode(this.PreviewMode);
                    break;

                case nameof(FramesPreprocessed):
                    var lastPreprocessedFrame = Math.Max(0, this.FramesPreprocessed - 1);
                    this.LastProcessedTimecode = this.ProcessingClip.MarkInTimecode.AddFrames(lastPreprocessedFrame);
                    break;

                case nameof(FramesProcessed):
                    var lastProcessedFrame = Math.Max(0, this.FramesProcessed - 1);
                    this.LastProcessedTimecode = this.ProcessingClip.MarkInTimecode.AddFrames(lastProcessedFrame);
                    break;
            }
        }

        private SimpleClip _sourceClip;

        public int FramesPreprocessed
        {
            get => GetCurrentValue<int>();
            private set => SetAndRaiseOnChange(value);
        }

        public int FramesProcessed
        {
            get => GetCurrentValue<int>();
            private set => SetAndRaiseOnChange(value);
        }

        private int _framesToProcess;

        public string PreprocessErrorMessage
        {
            get => GetCurrentNullableValue<string>();
            set => SetAndRaiseOnChange(value);
        }

        public bool PreviewMode
        {
            get => GetCurrentValue<bool>();
            set => SetAndRaiseOnChange(value);
        }

        public bool SaveFrames
        {
            get => GetCurrentValue<bool>();
            set => SetAndRaiseOnChange(value);
        }

        public string ProcessErrorMessage
        {
            get => GetCurrentNullableValue<string>();
            set => SetAndRaiseOnChange(value);
        }

        public string ProcessedSubfolder
        {
            get => GetCurrentNullableValue<string>();
            set => SetAndRaiseOnChange(value);
        }

        public ProcessingStatusEnum ProcessingStatus
        {
            get => GetCurrentValue<ProcessingStatusEnum>();
            set => SetAndRaiseOnChange(value);
        }

        public bool ReadyToPreprocess
        {
            get => GetCurrentValue<bool>();
            set => SetAndRaiseOnChange(value);
        }

        public SimpleClip ProcessingClip
        {
            get => GetCurrentNullableValue<SimpleClip>();
            set => SetAndRaiseOnChange(value);
        }

        public SimpleClip SourceClip
        {
            get => _sourceClip;

            set
            {
                if (this.ProcessingStatus == ProcessingStatusEnum.Processing)
                {
                    throw new Exception("Cannot change clips while processing");
                }

                if (this.SourceClip != null)
                {
                    this._sourceClip.PropertyChanged -= this.SourceClipOnPropertyChanged;
                }

                this._sourceClip = value;
                if (this._sourceClip != null)
                {
                    this._sourceClip.PropertyChanged += this.SourceClipOnPropertyChanged;
                    this.ProcessingClip = this._sourceClip.Copy();
                }

                this.RaisePropertyChanged();
            }
        }

        private void SourceClipOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.ProcessingStatus == ProcessingStatusEnum.Processing)
            {
                return;
            }

            switch (e.PropertyName)
            {
                case nameof(SimpleClip.MarkInTimecode):
                    this.ProcessingClip.MarkInTimecode = this.SourceClip.MarkInTimecode;
                    this.ForcePropertyChanged("MarkedFrames");
                    break;

                case nameof(SimpleClip.MarkOutTimecode):
                    this.ProcessingClip.MarkOutTimecode = this.SourceClip.MarkOutTimecode;
                    this.ForcePropertyChanged("MarkedFrames");
                    break;
            }
        }

        public int Pass { get; set; }

        public int Passes
        {
            get => GetCurrentValue<int>();
            set => SetAndRaiseOnChange(value);
        }

        public MtiRoi<int> ProcessRoi
        {
            get => GetCurrentValue<MtiRoi<int>>();
            set => SetAndRaiseOnChange(value);
        }

        public SimpleTimecode LastProcessedTimecode
        {
            get => GetCurrentNullableValue<SimpleTimecode>();
            private set => SetAndRaiseOnChange(value);
        }

        public int DispatchCommands(ToolProcessingCommandEnum toolProcessingCommand)
        {
            switch (toolProcessingCommand)
            {
                case ToolProcessingCommandEnum.StartPreprocess:
                    this.Pass = 0;
                    // NativeMethodsTestBed.InitToolProcessing();
                    return this.BeginPreprocess();

                case ToolProcessingCommandEnum.CancelPreprocess:
                    return this.CancelPreprocess();

                case ToolProcessingCommandEnum.StartProcess:
                    // NativeMethodsTestBed.InitToolProcessing();
                    return this.BeginProcess();

                case ToolProcessingCommandEnum.CancelProcess:
                    return this.CancelProcess();

                case ToolProcessingCommandEnum.SetPreviewModeOn:
                    return this.SetPreviewMode(true);

                case ToolProcessingCommandEnum.SetPreviewModeOff:
                    return this.SetPreviewMode(false);
            }

            return -1;
        }

        private int SetPreviewMode(bool view)
        {
            this.PreviewMode = view;
            return 0;
        }

        private int CancelPreprocess()
        {
            this._preprocessingCanceled = true;
            this._preprocessBackgroundWorker.CancelAsync();
            NativeMethodsTestBed.CancelPreprocessingAsync();
            return 0;
        }

        // TODO: freeze passes
        private int BeginPreprocess()
        {
            try
            {
                if (this.ProcessingClip.MarksAreValid() == false)
                {
                    this.PreprocessErrorMessage = "Invalid marks";
                    return -1;
                }

                this.PreprocessErrorMessage = "Processing";
                this._preprocessingCanceled = false;
                this.FramesPreprocessed = 0;

                // ReSharper disable once PossibleInvalidOperationException
                this._framesToProcess = this.ProcessingClip.MarkedFrames.Value;

                // First we call the processing init
                // If return is false, we report error and don't continue
                this.ProcessingStatus = ProcessingStatusEnum.Processing;

                var status =
                    NativeMethodsTestBed.InitPreprocessing(this.Pass, ProcessingClip.GetMarkedFileNames(), this.ProcessRoi,
                        this._framesToProcess);
                if (status != 0)
                {
                    this.PreprocessErrorMessage = "Init of tool processing failed";
                    return status;
                }
            }
            catch (Exception e)
            {
                this.PreprocessErrorMessage = e.Message;
                return -1;
            }

            this._preprocessBackgroundWorker.RunWorkerAsync();
            return 0;
        }

        private readonly BackgroundWorker _preprocessBackgroundWorker = new BackgroundWorker()
        { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

        private readonly BackgroundWorker _processBackgroundWorker = new BackgroundWorker()
        { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

        private bool _preprocessingCanceled;

        private void InitializeBackgroundWorkers()
        {
            this._preprocessBackgroundWorker.DoWork += PreprocessBackgroundWorkerOnDoWork;
            this._preprocessBackgroundWorker.RunWorkerCompleted += PreprocessBackgroundWorkerOnRunWorkerCompleted;
            this._preprocessBackgroundWorker.ProgressChanged += PreprocessBackgroundWorkerOnProgressChanged;

            this._processBackgroundWorker.DoWork += ProcessBackgroundWorkerOnDoWork;
            this._processBackgroundWorker.RunWorkerCompleted += ProcessBackgroundWorkerOnRunWorkerCompleted;
            this._processBackgroundWorker.ProgressChanged += ProcessBackgroundWorkerOnProgressChanged;
        }


        private void PreprocessBackgroundWorkerOnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var action = e.UserState as string;
            if (action == "FramesPreprocessed")
            {
                this.FramesPreprocessed = e.ProgressPercentage;
            }
        }

        private void PreprocessBackgroundWorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            var worker = sender as BackgroundWorker;
            NativeMethodsTestBed.PreprocessingCompleted(this.Pass);
            this.ProcessingStatus = (ProcessingStatusEnum)e.Result;

            // If cancel process, exit otherwise try another pass
            if (_preprocessingCanceled == false)
            {
                this.Pass++;
                if (this.Pass < this.Passes)
                {
                    this.BeginPreprocess();
                }
            }
        }

        private void PreprocessBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            var worker = sender as BackgroundWorker;
            try
            {
                foreach (var i in Enumerable.Range(0, this._framesToProcess))
                {
                    string fileName = ProcessingClip.FullFileNameFromMarkIn(i);
                    int status = NativeMethodsTestBed.PreprocessFrame(this.Pass, fileName, ProcessRoi, i);
                    worker?.ReportProgress(i + 1, "FramesPreprocessed");
                    if (worker?.CancellationPending == true)
                    {
                        e.Result = ProcessingStatusEnum.UserAborted;
                        Application.Current?.Dispatcher?.Invoke(() => this.PreprocessErrorMessage = "User Aborted");
                        return;
                    }
                }
            }
            catch (Exception exception)
            {
                Application.Current?.Dispatcher?.Invoke(() => this.PreprocessErrorMessage = exception.Message);
                e.Result = ProcessingStatusEnum.Failure;
                return;
            }

            Application.Current?.Dispatcher?.Invoke(() => this.PreprocessErrorMessage = "Success");
            e.Result = ProcessingStatusEnum.Success;
        }

        private int BeginProcess()
        {
            try
            {
                if (this.ProcessingClip.MarksAreValid() == false)
                {
                    this.ProcessErrorMessage = "Invalid marks";
                    return -1;
                }

                // ReSharper disable once PossibleInvalidOperationException
                this._framesToProcess = this.ProcessingClip.MarkedFrames.Value;

                // First we call the processing init
                // If return is false, we report error and don't continue
                var startFileName = this.ProcessingClip.FullFileName(this.ProcessingClip.MarkInTimecode);
                this.ProcessingStatus = ProcessingStatusEnum.Processing;

                var status =
                    NativeMethodsTestBed.InitProcessing(ProcessingClip.GetMarkedFileNames(), this.ProcessRoi,
                        this._framesToProcess, this.SaveFrames, this.ProcessedSubfolder);
                if (status != 0)
                {
                    this.ProcessErrorMessage = "Init of tool processing failed";
                    return status;
                }
            }
            catch (Exception e)
            {
                this.ProcessErrorMessage = e.Message;
                return -1;
            }

            this._processBackgroundWorker.RunWorkerAsync();
            return 0;
        }

        private int CancelProcess()
        {
            this._processBackgroundWorker.CancelAsync();
            NativeMethodsTestBed.CancelProcessingAsync();

            return 0;
        }

        private void ProcessBackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            var worker = sender as BackgroundWorker;
            try
            {
                foreach (var i in Enumerable.Range(0, this._framesToProcess))
                {
                    string fileName = ProcessingClip.FullFileNameFromMarkIn(i);
                    int status = NativeMethodsTestBed.ProcessFrame(fileName, ProcessRoi, i);
                    worker?.ReportProgress(i + 1, "FramesProcessed");
                    if (worker?.CancellationPending == true)
                    {
                        e.Result = ProcessingStatusEnum.UserAborted;
                        Application.Current?.Dispatcher?.Invoke(() => this.ProcessErrorMessage = "User Aborted");
                        return;
                    }
                }
            }
            catch (Exception exception)
            {
                Application.Current?.Dispatcher?.Invoke(() => this.ProcessErrorMessage = exception.Message);
                e.Result = ProcessingStatusEnum.Failure;
                return;
            }

            Application.Current?.Dispatcher?.Invoke(() => this.ProcessErrorMessage = "Success");
            e.Result = ProcessingStatusEnum.Success;
        }


        private void ProcessBackgroundWorkerOnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            var action = e.UserState as string;
            if (action == "FramesProcessed")
            {
                this.FramesProcessed = e.ProgressPercentage;
            }
        }

        private void ProcessBackgroundWorkerOnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Get the BackgroundWorker that raised this event.
            var worker = sender as BackgroundWorker;
            NativeMethodsTestBed.ProcessingCompleted();
            this.ProcessingStatus = (ProcessingStatusEnum)e.Result;
        }
    }
}

