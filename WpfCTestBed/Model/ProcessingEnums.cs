﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfCTestBed.Model
{
    public enum ProcessingStatusEnum
    {
        /// <summary>
        /// No hash
        /// </summary>
        [Description("Unknown")] Unknown = 0,

        [Description("Success")] Success = 1,

        [Description("Failure")] Failure = 2,

        [Description("User Aborted")] UserAborted = 3,

        [Description("Processing")] Processing = 4
    }

    public enum ToolProcessingCommandEnum
    {
        [Description("Unknown")] Unknown = 0,

        [Description("Start Preprocess")] StartPreprocess = 1,

        [Description("Abort Preprocess")] CancelPreprocess = 2,

        [Description("Start Process")] StartProcess = 3,

        [Description("Abort Process")] CancelProcess = 4,

        [Description("Turn on Preview")] SetPreviewModeOn = 5,

        [Description("Turn off Preview")] SetPreviewModeOff = 6,
    }
}
