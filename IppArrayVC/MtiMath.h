#pragma once
#ifndef MTIMATHH
#define MTIMATHH
#include <vector>
#include "Ippheaders.h"

using std::vector;

// This is a catch all for mathematical routines that are static
// and C data types.  Not IppArray data.  Those belong in a higher class
//
// and often wrappers around external routines as MKL routines
class Ipp32fArray;

namespace MtiMath 
{
	template <typename T> int signum(T val) {
		return (T(0) < val) - (val < T(0));
	}

	int lubksb_row_major(float *a, int n, int *ipiv, float *b);
	int ludcmp_row_major(int n, float* a, int lda, int *ipiv);

	int lubksb_row_major(double *a, int n, int *ipiv, double *b);
	int ludcmp_row_major(int n, double* a, int lda, int *ipiv);
	
	// These are only a partial implementation  
	MTI_IPPARRAY_API vector<double> pchipslopes(const vector<double> &x, const vector<double> &y, const vector<double> &del);
	MTI_IPPARRAY_API vector<double> pchipslopes(const vector<double> &x, const vector<double> &y);

	MTI_IPPARRAY_API vector<double> pchip(const vector<double> &x, const vector<double> &y, const vector<double> &t);
	MTI_IPPARRAY_API vector<double> standardizeLut(const vector<double> &means, const vector<double> &sds);

	MTI_IPPARRAY_API std::tuple<double, double, int> meanAndSdFromHistogram(const vector<int> &counts, const double maxValue, double defaultMean=0, double defaultStdev=0);

	inline vector<int>linspace(int start, int end, int size)
	{
		if (size <= 1)
		{
			return { start };
		}

		vector<int>result(size);

		const auto delta = (double(end) - double(start)) / (double(size) - 1.0);
		for (auto k = 0; k < size; k++)
		{
			result[k] = int(std::round(start + k * delta));
		}

		return result;
	}

	template<class T>
    vector<T>linspace(T start, T end, int size)
	{
		if (size <= 1)
		{
			return { start };
		}

		vector<T> result(size);

		const auto delta = (double(end) - double(start)) / (double(size) - 1.0);
		for (auto k = 0; k < size; k++)
		{
			result[k] = T(start + k * delta);
		}

		return result;
	}
}

#endif
