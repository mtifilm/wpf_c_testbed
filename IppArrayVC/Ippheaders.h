#ifndef IppHeadersH
#define IppHeadersH

// This is designed to speed up the compiling
// by using precompiled header

#include <sstream>
#include <ostream>
#include <vector>
#include "ipp.h"

#include "IppArrayLibInt.h"
#include "MtiMath.h"
#include "MtiIppExtensions.h"
#include "IppException.h"
#include "MtiRuntimeError.h"
#include "IppArray.h"
#include "Ipp32fArray.h"
#include "Ipp32sArray.h"
#include "Ipp16uArray.h"
#include "Ipp8uArray.h"
#include "IppMatIO.h"
#include "MtiPlanar.h"
#include "FastHistogramIpp.h"
#endif