#include "polyRegularize.h"
#include "FastHistogramIpp.h"
#include "Array2D.h"
#include "MtiMath.h"

namespace MtiMath
{
	vector<double> solve(const Array2D<double> &matrix, const vector<double> &Y)
	{
		auto m = matrix.rows();
		auto n = matrix.columns();
		if (n != m)
		{
			THROW_MTI_RUNTIME_ERROR("Only square (nxn) matrices supported");
		}

		if (n != int(Y.size()))
		{
			THROW_MTI_RUNTIME_ERROR("Rows sizes don't match");
		}

		// make a copy
		Array2D<double> tempArray(matrix);

		vector<int> pivot(n);
		auto status = MtiMath::ludcmp_row_major(n, tempArray.data(), n, pivot.data());
		if (status != 0)
		{
			std::stringstream os;
			os << "Error in computing L " << status;
			THROW_MTI_RUNTIME_ERROR(os.str());
		}

		auto result = Y;
		status = MtiMath::lubksb_row_major(tempArray.data(), n, pivot.data(), result.data());
		if (status != 0)
		{
			std::stringstream os;
			os << "Error in solving " << status;
			THROW_MTI_RUNTIME_ERROR(os.str());
		}

		return result;
	}

	Ipp16uArray  polyRegularize(const Ipp16uArray &aIn, const Ipp16uArray &bIn, int mm, int MM, double epsilon, int order)
	{
		// NOTE ON PARAMETERS:  I'VE HAD NO PROBLEMS USING 
		//
		// epsilon=.0001
		// order=5 or order=4 (order of the polynomial fit for the central range of
		//                     lut values)
		//
		// (Actually, it's very stable to both order and epsilon.  From what I've
		// seen of this version, epsilon can go smaller and order can go higher with
		// very little change.  But for sure there's trouble if epsilon is zero
		// (since the solution can float up or down) and there has to be trouble at
		// very high orders, though even order 10 behaves very well.)
		//
		// mm and MM are the minimum and maximum intensities over which there is
		// good data, meaning large sample.  These can be set quite aggressively
		// (maximize the range).
		//
		// a and b have length equal to the number of intensity levels
		// a(n) is the original intensity level that achieves the third 
		// quartile of the neighbors of n, and b(n) is the intensity that 
		// achieves the first quartile.  a(n) and b(n) are set to zero for n out
		// of range of useful data (see m and M, below).
		//

		// Let N be the total number of intensities 
		MTIassert((aIn.getHeight() == 1) && (aIn.getComponents() == 1));
		MTIassert(aIn.getSize() == bIn.getSize());

		// N = numel(a);
		auto N = aIn.getWidth();

		// Need to put a and be on the same scale as will be used during the
		// computations the lut, l.It's easiest to make the initial value for l be
		// one: l(n) = 1, so that, e.g., '5', refers to the gray level 4.

		//	a = a + 1;
		auto a = aIn + 1;

		//b = b + 1;
		auto b = bIn + 1;

		// At the end, the lut values are decremented by 1 so that the first value
		// is zero and the last is N - 1.

		// The maximum gap is the maximum over all n between mm and MM of the
		// gap between a(n) and n and between n and b(n).It is used to define the
		// range of n that will contribute to the cost function.

		// max_gap = round(max(max(a(mm:MM) - (mm:MM)'), max((mm:MM)' - b(mm:MM))));
		auto maxGap = 0.0f;
		for (auto i = mm; i <= MM; i++)
		{
			maxGap = std::max<float>(maxGap, float(a[i] - i));
			maxGap = std::max<float>(maxGap, float(i - b[i]));
		}

		//  The values of n from m to M(defined next) each contribute a term in the
		//	cost function, which tries to minimize the difference between the average
		//	gap and each of the above and below gaps for this set of n.  (This does
		//	not mean that the lut, l, at other values of n are not changed in
		//	 minimizing the cost.Some of
		//	these enter because they are the position pointed to by a or b for some
		//	values within the range m to M.)

		// m = mm + max_gap;
		// M = MM - max_gap;

		auto m = mm;
		auto M = MM;

		// Will only use the "above" and "below" values for n = m, m + 1, ..., M.
		for (auto i = 0; i < m; i++)
		{
			a[i] = 0;
			b[i] = 0;
		}

		for (auto i = M+1; i < N; i++)
		{
			a[i] = 0;
			b[i] = 0;
		}

		//  Referring to a and b following the substitution of these zeros :
		//  na(n) is the number of intensities i for which n = a(i), some value i, and
		//	nb(n) is the number for which n = b(i).Either or both could be zero.

		//	A(n, k), k = 1, 2, ..., na(n), is the value of the k'th intensity i for which 
		//	a(i) = n.
		//	B(n, k), k = 1, 2, ..., nb(n), is the value of the k'th intensity i for which
		//	b(i) = n.

		//	Thus a(A(n, j)) = b(B(n, k)) = n, for allowed values of j and k.

		//	l is a vector of length N.The goal is to compute l(n),
		//	n = 1, 2, ..., N, so as to make all of the gaps, to the above and below
		//	values(a(n) and b(n)), across all intensities, as nearly equal as
		//	possible.

		//	Gaps are made close by making them all close to a common number, for
		//	which we take the average gap over all active elements of l, when l is
		//	the identity lut :

		//G = (sum(a(m:M) - (m:M)') + sum((m:M)' - b(m:M))) / (2 * (M - m) + 2);
		auto g = 0.0f;
		vector<int> diffs;
		for (auto i = mm; i <= MM; i++)
		{
			auto d = a[i] - b[i];
			diffs.push_back(d);
			g += a[i] - b[i];
		}

		g = g / (2 * (M - m) + 2);

		//  The new gap, after applying l, is(l(a(n)) - l(n)), for the a values,
		//	and (l(n) - l(b(n))) for the b values.The cost is the sum over all
		//	legitimate gaps(see remarks above about m and M) of the square of the
		//	gap minus G : sum(gap - G) ^ 2 .

		//	The minimizer is not unique(e.g.same cost by adding a constant to
		//	every element of l, and there may be other ways to preserve
		//	cost, I'm not sure).  So cost is "regularized" by adding 
		//	epsilon || l(m:M) - (m:M) || ^ 2

		//	In this version(as opposed to solve - regularized.m), l(m:M) is directly
		//	fit with a polynomial of order 'order' (power of highest term).

		//	The cost is quadratic, so solution is by matrix inverse.

		//	The last step is to do extension from m down to 1 and from M up to N, by
		//	using third - order polynomials that require l(1) = 1, l(N) = N, and matches of
		//	values and derivatives at m and M.

		//		  Initializations

		//na = zeros(1, N);
		//nb = zeros(1, N);
		//A = zeros(N, N);
		//B = zeros(N, N);

		// Construct the inverse functions, A and B

		// count = 1:N;
		// for n = 1:N
		//	places = (a == n);
		//indices = count(places);
		//na(n) = sum(places);
		//A(n, 1:na(n)) = indices;

		//places = (b == n);
		//indices = count(places);
		//nb(n) = sum(places);
		//B(n, 1:nb(n)) = indices;

		//end

		vector<vector<int>> A(N + 1);
		vector<vector<int>> B(N + 1);
		for (auto i : range(N))
		{
			A[a[i]].push_back(i+1);
			B[b[i]].push_back(i+1);
		}

		vector<int> na(N+1);
		vector<int> nb(N+1);

		for (auto i = 0 ; i <= N; i++)
		{
			na[i] = int(A[i].size());
			nb[i] = int(B[i].size());
		}

		//	A and B are big.Make them as small as possible :

		//mosta = max(na);
		//mostb = max(nb);

		//A = A(:, 1 : mosta);
		//B = B(:, 1 : mostb);

		// uild the matrix(T) and vector(V) of the linear system that solves for
		//	the polynomial coefficients c = (c(1), c(2), ..., c(order + 1)) corresponding to
		//	exponents 0, 1, ..., order.This polynomial will be used for the fit on the
		//	interval m to M.But it is computed using a change of variables, l(n) is
		//	given by the polynomial evaluated at the standardized value of n on the
		//	interval m to M.

		//	% Initialize T and V

		//	O = order + 1;   % Capital o for order + 1
		auto order1 = order + 1;

		//	T = zeros(O, O);
		Array2D<double> t00(order1, order1);
		t00.set(0.0);

		//V = zeros(O, 1);
		vector<Ipp64f> V(order1, 0.0);

		// Polynomial will be in terms of centered and scaled value of n, which
		// might improve stability

		//	mu = (N + 1) / 2;
		auto mu = (N + 1) / 2.0;

		//sigma2 = N * (N + 1)*(2 * N + 1) / (6 * N) - mu ^ 2;
		auto sigma2 = N * (N + 1.0)*(2.0 * N + 1) / (6.0 * N) - (mu *mu);

		// sigma = sqrt(sigma2);
		auto sigma = sqrt(sigma2);

		//x = ((1:N) - mu) / sigma;
		vector<Ipp64f> x(N + 1);
		for (auto i : range(x.size()))
		{
			x[i] = (i - mu) / sigma;
		}
	
		//% Fill in the vector V
		////	for q = 1:O
		for (auto q : range(order1))
		{
			// qexp = q - 1;   // annoying problem with indexing not starting at zero
			auto qexp = q;

			//  this is the exponent in the polynomial
			//		for n = 1:m - 1
			for (auto n : range(1, m+1))
			{
			//	V(q) = V(q) - G * nb(n)*x(n) ^ qexp;
				V[q] = V[q] - g * B[n].size()*pow(x[n], qexp);
			}
			//end

			//	for n = M + 1:N
			for (auto n : range(M + 2, N + 1))
			{
				//V(q) = V(q) + G * na(n)*x(n) ^ qexp;
				V[q] = V[q] + g * A[n].size()*pow(x[n], qexp);
			}
			//end

			//	for n = m:M
			for (auto n : range(m+1, M + 2))
			{
				auto nab = (na[n] - nb[n]);
				auto p = pow(x[n], qexp);
				auto f = epsilon * n*pow(x[n], qexp);
			//	V(q) = V(q) + G * (na(n) - nb(n))*x(n) ^ qexp + epsilon * n*x(n) ^ qexp;
				V[q] = V[q] + g * (na[n] - nb[n])*pow(x[n], qexp) + epsilon * n*pow(x[n], qexp);
			}
			//end
		}
		//	end
		//MatIO::saveListToMatFile(R"(c:\temp\proxyMins.mat)", V, "VC");
		//	Fill in the matrix T

		//	for q = 1:O
		for (auto q : range(order1))
		{
			//		for r = 1 : O
			for (auto r : range(order1))
			{
				//		qexp = q - 1;   // annoying problem with indexing not starting at zero
				auto qexp = q;
				//		 this is the exponent in the polynomial
				//			rexp = r - 1;   % annoying problem with indexing not starting at zero
				auto rexp = r;
				//			 this is the exponent in the polynomial

				//			for n = 1:m - 1
				for (auto n : range(1, m+1))
				{
					// Hold = 0;
					auto hold = 0.0;

					// for k = 1:nb(n)
					for (auto k : range(B[n].size()))
					{
						//	Hold = Hold + x(B(n, k)) ^ rexp;
						hold += pow(x[B[n][k]], rexp);
					}
					//end

				//	t(q, r) = t(q, r) + x(n) ^ qexp*(nb(n)*x(n) ^ rexp - hold);
					t00[{ q, r }] += pow(x[n], qexp) * (nb[n] * pow(x[n], rexp) - hold);
				}
				////end

				// for n = M + 1:N
				for (auto n : range(M + 2, N + 1))
				{

					// Hold = 0;
					auto hold = 0.0;

					// for k = 1:na(n)
					for (auto k : range(na[n]))
					{
						// Hold = Hold + x(A(n, k)) ^ rexp;
						hold += pow(x[A[n][k]], rexp);
					}
					//end

					// T(q, r) = T(q, r) + x(n) ^ qexp*(na(n)*x(n) ^ rexp - Hold);
					t00[{ q, r }] += pow(x[n], qexp)*(na[n] * pow(x[n], rexp) - hold);
				}
				//end

				////	for n = m:M
				for (auto n : range(m+1, M+2))
				{
					// Holdb = 0;
					auto holdb = 0.0;

					//for k = 1:nb(n)
					for (auto k : range(nb[n]))
					{
						//	Holdb = Holdb + x(B(n, k)) ^ rexp;
						holdb += pow(x[B[n][k]], rexp);
					}
					//end

				//	Holda = 0;
					auto holda = 0.0;
					//for k = 1:na(n)
					for (auto k : range(na[n]))
					{
						// Holda = Holda + x(A(n, k)) ^ rexp;
						holda += pow(x[A[n][k]], rexp);
					}
					//end

					// (na(n) + nb(n) + 2 + epsilon) * x(n) ^ rexp ...
					auto f1 = (na[n] + nb[n] + 2 + epsilon) * pow(x[n], rexp);

					//	-x(b(n)) ^ rexp - x(a(n)) ^ rexp
					//auto bn0 = b[n-1];
					//auto an0 = a[n-1];
					//auto bp = pow(x[b[n-1]], rexp);
					auto f2 = -pow(x[b[n-1]], rexp) - pow(x[a[n-1]], rexp);

					//	T(q, r) = T(q, r) + x(n) ^ qexp * (...
					//t00[{q, r}] += pow(x[n], qexp) * (f1 + f2); // +f2 - holdb - holda);
					t00[{ q, r }] += pow(x[n], qexp) * (f1 + f2 - holdb - holda);
					//end
				}
			}
			//	end    % end r loop
		}
		//	end    % end q loop

		//	% Solve for the polynomial coefficients('c') :

		//	c = T ^ (-1)*V;
		Ipp32fArray T({ order1 ,order1 });
		for (auto i : range(T.area()))
		{
			T[i] = float(t00[i]);
		}

		Ipp32fArray V32f(order1);
		for (auto i:range(order1))
		{
			V32f[i] = float(V[i]);
		}

		//auto c = T.solve(V32f);
		//auto ct = c.toVector()[0];
		auto cv = solve(t00, V);
//		MatIO::saveListToMatFile(R"(c:\temp\proxyMins.mat)", V32f, "VC", T, "TC", na, "naC", nb, "nbC", x, "xC");
		// Fill in lut for n = m, ..., M

		//	l = zeros(1, N);
		Ipp32fArray lut(N);
		lut.zero();

		//for n = m:M
		for (auto n : range(m, M + 1))
		{
			//	for q = 1 : O
			for (auto q : range(order1))
			{
				//	l(n) = l(n) + c(q)*x(n) ^ (q - 1);
				lut[n] += float(cv[q] * pow(x[n+1], q));
			}
		}

		//end

		//	end

		//	Now extrapolate with two quadratic functions, one for each end.These
		//	are determined by matching the derivative and value at m(or M), and then
		//	demanding that l(1) = 1 (or l(N) = N). (But all of this is done in the scaled
		//	x values, and later turned back to integer gray levels.)

		//	Equation is Ae = f, where e has the coefficients of the extrapolating
		//	polynomial.

		//	extrapolate for n from 1 to m(i.e.extrapolate to x(1), ..., x(m))

		//	A = zeros(3, 3);
		auto E = Array2D<double>({ 3,3 });

		//f = zeros(3, 1);
		auto f = vector<double>(3);

		// make extrapolation agree with l at n = m
		//	A(1, 1) = 1;
		E[{ 0, 0 }] = 1;

		//A(1, 2) = x(m);
		E[{ 1, 0 }] = x[m+1];

		//A(1, 3) = x(m) ^ 2;
		E[{ 2, 0 }] = x[m+1] * x[m+1];

		//f(1) = 0;
		f[0] = 0;

		//for q = 1:O
		for (auto q : range(order1))
		{
			//	f(1) = f(1) + c(q)*x(m) ^ (q - 1);
			f[0] += cv[q] * pow(x[m+1], q);
		}
		//end

		// make l(1) = 1
		//	A(2, 1) = 1;
		E[{ 0, 1 }] = 1;

		//A(2, 2) = x(1);
		E[{ 1, 1 }] = x[1];

		//A(2, 3) = x(1) ^ 2;
		E[{ 2, 1 }] = x[1] * x[1];

		f[1] = 1;

		//% match derivative of extrapolation to derivative of l at n = m
		//	A(3, 1) = 0;
		E[{ 0, 2 }] = 0;

		//A(3, 2) = 1;
		E[{ 1, 2 }] = 1;

		//A(3, 3) = 2 * x(m);
		E[{ 2, 2 }] = 2 * x[m+1];

		//f(3) = 0;
		f[2] = 0;

		//for q = 2:O
		for (auto q : range(order1))
		{
			//	f(3) = f(3) + c(q)*(q - 1)*x(m) ^ (q - 2);
			f[2] = f[2] + cv[q] * q * pow(x[m+1], q - 1);
		}
		//end

		//	% solve for coefficients
		//	e = A ^ (-1)*f;
		auto e = solve(E, f);

		//% fill in lut for n < m

		//	for n = 1:m - 1
		for (auto n : range(m))
		{
			// l(n) = e(1) + e(2) * x(n) + e(3) * x(n) ^ 2;
			 lut[n] = Ipp16u(e[0] + e[1] * x[n+1] + e[2] * x[n+1] * x[n+1]);
		}
		//end

		//	% extrapolate for n from M to M + 1 (i.e.extrapolate to x(M), ..., x(N))

		//	A = zeros(3, 3);
		// f = zeros(3, 1);

		//% make extrapolation agree with l at n = M

		//	A(1, 1) = 1;
		E[{0, 0}] = 1;

		//A(1, 2) = x(M);
		E[{1, 0}] = x[M+1];

		//A(1, 3) = x(M) ^ 2;
		E[{2, 0}] = x[M+1] * x[M+1];

		//f(1) = 0;
		f[0] = 0;

		//for q = 1:O
		for (auto q : range(order1))
		{
			//	f(1) = f(1) + c(q)*x(M) ^ (q - 1);
			auto cq = cv[q];
			auto p = pow(x[M + 1], q);
			f[0] += cv[q] * pow(x[M+1], q);
		}
		//end

		//	% make l(N) = N

		//A(2, 1) = 1;
		//A(2, 2) = x(N);
		//A(2, 3) = x(N) ^ 2;
		E[{0, 1}] = 1;
		E[{1, 1}] = x[N];
		E[{2, 1}] = x[N] * x[N];

		//f(2) = N;
		f[1] = N;

		//% match derivative of extrapolation to derivative of l at n = m

		//	A(3, 1) = 0;
		E[{0, 2}] = 0;

		//A(3, 2) = 1;
		E[{1, 2}] = 1;

		//A(3, 3) = 2 * x(M);
		E[{2, 2}] = 2 * x[M+1];

		//f(3) = 0;
		f[2] = 0;

		//for q = 2:O
		for (auto q : range(1, order1))
		{
			//	f(3) = f(3) + c(q)*(q - 1)*x(M) ^ (q - 2);
			f[2] += cv[q] * q * pow(x[M+1], q-1);
		}
		//end

		//	% solve for coefficients

		//	e = A ^ (-1)*f;
		e = solve(E, f);

		//% fill in lut for n < m

		//	for n = M + 1:N
		for (auto n : range(M+1, N))
		{
			//l(n) = e(1) + e(2)*x(n) + e(3)*x(n) ^ 2;
			lut[n] = Ipp16u(e[0] + e[1] * x[n+1] + e[2] * x[n+1] * x[n+1]);
		}
		//end


		Ipp32fArray EArray({ 3 ,3 });
		for (auto i : range(EArray.area()))
		{
			EArray[i] = float(E[i]);
		}

		// Force monotonicity

		// l = max(1, l);
		// l = min(N, l);
		for (auto n : range(N))
		{
			lut[n] = std::max<float>(0, lut[n]);
			lut[n] = std::min<float>(N - 1.0f, lut[n]);
		}

		//for n = 2:N
		for (auto n : range(1, N))
		{
			//	l(n) = max(l(n), l(n - 1));
			lut[n] = round(std::max<float>(lut[n], lut[n - 1]));
		}
		//end

		//	% Fix l to be integer and to have range[0, N - 1] instead of[1, N]
		MatIO::saveListToMatFile(R"(c:\temp\proxyMins.mat)", EArray, "EC", lut, "lutC", f, "fC", e, "eC");

		//	l = round(l) - 1;
		return Ipp16uArray(lut);
	}
}
