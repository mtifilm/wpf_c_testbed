#pragma once
#ifndef  MtiAncillaryTemplateH
#define MtiAncillaryTemplateH

#include "Ippheaders.h"

// These are templates for Ipp??Array.  They are hidden from the actual interface
// So we don't have to recompile the world.  
// We could be more efficient here
template<typename T>
void circularFill(const T &result, const T &inputArray, const MtiRect &probeRoi)
{
	const MtiRect baseRoi = { {0,0} , inputArray.getSize() };
	const auto validRoi = baseRoi & probeRoi;
	auto factors = probeRoi % validRoi;
	auto zeroFactors = factors.zeroBias();

	result(zeroFactors.tl) <<= inputArray(factors.tl + IppiPoint{ baseRoi.width, baseRoi.height });
	result(zeroFactors.tr) <<= inputArray(factors.tr + IppiPoint{ -baseRoi.width, baseRoi.height });
	result(zeroFactors.bl) <<= inputArray(factors.bl + IppiPoint{ baseRoi.width, -baseRoi.height });
	result(zeroFactors.br) <<= inputArray(factors.br + IppiPoint{ -baseRoi.width, -baseRoi.height });
	result(zeroFactors.cl) <<= inputArray(factors.cl + IppiPoint{ baseRoi.width, 0 });
	result(zeroFactors.cr) <<= inputArray(factors.cr + IppiPoint{ -baseRoi.width, 0 });
	result(zeroFactors.tc) <<= inputArray(factors.tc + IppiPoint{ 0, baseRoi.height });
	result(zeroFactors.bc) <<= inputArray(factors.bc + IppiPoint{ 0, -baseRoi.height });
}

template<typename T>
T SelectFromFunction(const T &inputArray, const MtiRect &probeRoi, MtiSelectMode selectMode, bool utilizeExtrinsicData)
{
	// If we want to utilize all the data, just call with ROI removed
	if (utilizeExtrinsicData)
	{
		auto baseArray = inputArray.baseArray();
		return baseArray(probeRoi + inputArray.getRoi().tl(), selectMode, false);
	}

	// The probeRoi is always relative to this roi
	const MtiRect virtualRoi({ 0,0 }, inputArray.getSize());

	// Early out, cheap copy
	// This will throw an error if probeRoi is not contained in large
	if ((selectMode == MtiSelectMode::valid) || virtualRoi.contains(probeRoi))
	{
		return inputArray(probeRoi);
	}

	// This is the information to copy
	auto validSourceRoi = virtualRoi & probeRoi;

	// Intersection is simple
	if (selectMode == MtiSelectMode::intersect)
	{
		return inputArray(validSourceRoi);
	}

	// This code could be refactored to extract the common term validTargetRoi.
	// Replication is pretty easy, copy over good data
	if (selectMode == MtiSelectMode::replicate)
	{
		// Check if data is available
		if (validSourceRoi.isEmpty())
		{
			THROW_MTI_RUNTIME_ERROR("No data available for replication");
		}

		T result({ probeRoi.getSize(), inputArray.getComponents() });
		result.setOriginalBits(inputArray.getOriginalBits());
		auto validTargetRoi = result.getRoi() & (validSourceRoi - probeRoi.tl());
		result(validTargetRoi) <<= inputArray(validSourceRoi);
		result.duplicateFill(validTargetRoi);
		return result;
	}

	// Mirror is hard, a faster method would be to build the mirror directly
	// but that is difficult
	if (selectMode == MtiSelectMode::mirror)
	{
		// Mirror works by filling an array with the valid data then reflecting that
		// Unfortunately if the probe intersection with valid data is too small, their won't
		// be enough data.  So build a big enough one and then cut it down
		MtiRect expandedProbe;
		MtiRect validProbe;

		std::tie(expandedProbe, validProbe) = expandTo50Percent(virtualRoi, probeRoi);
		if (expandedProbe == probeRoi)
		{
			// There is enough data
			T result({ probeRoi.getSize(), inputArray.getComponents() });
			auto validTargetRoi = result.getRoi() & (validSourceRoi - probeRoi.tl());
			result(validTargetRoi) <<= inputArray(validSourceRoi);
			result.setOriginalBits(inputArray.getOriginalBits());
			result.mirrorFill(validTargetRoi);
			return result;
		}

		auto result = SelectFromFunction(inputArray, expandedProbe, selectMode, utilizeExtrinsicData);
		return result(validProbe);
	}

	if (selectMode == MtiSelectMode::circular)
	{
		// Circular is very simple, the outside image is just copied from the far side
		T result({ probeRoi.getSize(), inputArray.getComponents() });
		auto validTargetRoi = result.getRoi() & (validSourceRoi - probeRoi.tl());
		result(validTargetRoi) <<= inputArray(validSourceRoi);
		result.setOriginalBits(inputArray.getOriginalBits());
		circularFill(result, inputArray, probeRoi);
		return result;
	}

	THROW_MTI_RUNTIME_ERROR("Unsupported copyMode")
}



// This is the usual disk, equivalent to matlab's strel('disk', radius, 0)
template<typename T>
T createRaggedDisk(int radius)
{
	auto diameter = 2 * radius + 1;
	T disk({ diameter, diameter });
	disk.zero();
	for (auto r = 0; r < disk.getHeight(); r++)
	{
		for (auto c = 0; c <= disk.getWidth(); c++)
		{
			const auto xd = radius - c;
			const auto yd = radius - r;
			const auto d = sqrt(xd*xd + yd * yd);
			if (d <= radius)
			{
				disk[{ c,r }] = 1;
			}
		}
	}

	return disk;
}

// This disk is equivalent to matlab's strel('disk', radius, 4)
template<typename T>
T createStrelDisk(int radius)
{
	// Algorithm breaks down above this level
	if (radius > 38)
	{
		THROW_MTI_RUNTIME_ERROR("Radius exceeds 38");
	}

	if (radius <= 2)
	{
		return createRaggedDisk<T>(radius);
	}

	// Why this? Go ask matlab
	if (radius == 3)
	{
		T result({ 5, 5 });
		result.set({ 1 });
		return result;
	}

	auto diameter = 2 * radius - 1;
	T disk({ diameter, diameter });
	disk.set({ Ipp8u(1) });

	int zeroLine[] = { 2, 2, 2, 2, 4, 4, 4 };
	const auto mi = radius - 4;
	const auto zeros = 4 * (mi / 7) + zeroLine[mi % 7];

	for (auto r = zeros; r >= 0; r--)
	{
		for (auto c = 0; c < r; c++)
		{
			auto row = zeros - r;
			disk[{ c, row }] = 0;
			disk[{ diameter - c - 1, row }] = 0;
			disk[{ c, diameter - row - 1 }] = 0;
			disk[{ diameter - c - 1, diameter - row - 1 }] = 0;
		}
	}

	return disk;
}


// tmp is size of rows*cols
template<typename T>
void boxFilterSum(T *in, int inPitchBytes, int rows, int cols, T *out, int outPitchBytes, const IppiSize &kernelSize, T *tmp)
{
	Ipp64f s00;
	auto kw = kernelSize.width;
	auto kh = kernelSize.height;

	// First do column sums of kernel height for each point.
	// Produce the first row
	MTImemcpy(tmp, in, cols * sizeof(T));
	for (auto r = 1; r < kernelSize.height; r++)
	{
		auto outr = tmp;
		auto inr = in + r * inPitchBytes / sizeof(T);
		for (auto c = 0; c < cols; c++)
		{
			*outr++ += *inr++;
		}
	}

	// Do each additional row
	for (auto r = 1; r < rows - kernelSize.height; r++)
	{
		auto outr = tmp + r * outPitchBytes / sizeof(T);
		auto inr = in + (r - 1) * inPitchBytes / sizeof(T);
		auto inrl = inr + kernelSize.height * inPitchBytes / sizeof(T);
		for (auto c = 0; c < cols; c++)
		{
			*outr -= *inr++;
			*outr++ += *inrl++;
		}
	}

	// Now partial sums of rows
	memcpy(out, tmp, cols * sizeof(T));
	for (auto r = 0; r < rows - kernelSize.height; r++)
	{
		auto outr = out + r * outPitchBytes / sizeof(T);
		auto inr = in + r * inPitchBytes / sizeof(T);

		auto s = 0.0f;
		for (auto c = 0; c < kernelSize.width; c++)
		{
			s += *(inr + c);
		}

		*outr++ = s;

		for (auto c = 1; c < cols - kernelSize.width; c++)
		{
			s -= *inr;
			s += *(inr + kernelSize.width);
			inr++;
			*outr += s;
		}
	}
}
//
//template<typename T>
//T reshapeTemplate(const T inputArray, const int rows, const MtiRect &roi)
//{
//	if ((roi.area() % rows) != 0)
//	{
//		THROW_MTI_RUNTIME_ERROR("Reshape must be possible, size not divisible by rows")
//	}
//
//	auto cols = roi.area() / rows;
//
//	T result({ cols, rows, inputArray.getComponents() });
//
//	auto bytesPerRow = roi.width * inputArray.getComponents() * inputArray.getDataSize();
//	for (auto r = 0; r < roi.height; r++)
//	{
//		auto p = inputArray.getElementPointer({ roi.x , r + roi.y });
//		auto q = result.getRowPointer(r);
//		memcpy(q, bytesPerRow, p);
//	}
//
//	return result;
//}
#endif
