#include "IppArray.h"

// This contains static helper routines

    IppDataType findDataType(const string &name)
	{
		if (typeid(Ipp32f).name() == name)
		{
			return ipp32f;
		}

		if (typeid(Ipp16u).name() == name)
		{
			return ipp16u;
		}

		if (typeid(Ipp32s).name() == name)
		{
			return ipp32s;
		}

		if (typeid(Ipp8u).name() == name)
		{
			return ipp8u;
		}

		// Add others here
		THROW_MTI_RUNTIME_ERROR("This type is not supported for the array");
	}

int findDataSize(IppDataType ippDataType)
{
	switch (ippDataType)
	{
	case ipp64u:
	case ipp64uc:
	case ipp64s:
	case ipp64sc:
	case ipp64f:
	case ipp64fc:
		return 8;

	case ipp32u:
	case ipp32uc:
	case ipp32sc:
	case ipp32fc:
	case ipp32s:
	case ipp32f:
		return 4;

	case ipp16uc:
	case ipp16s:
	case ipp16sc:
	case ipp16u:
		return 2;

	case ipp8s:
	case ipp8sc:
	case ipp8uc:
	case ipp8u:
		return 1;
	case ipp1u:
		return 0;

	default:
		return -1;
	}
}




