#include "IppArray.h"
#include "IpaStreamDataItem.h"

Ipp8uArray IpaStreamDataItem::getIpp8uArray() const
{
	// Check for item type
	if (getArrayType() == ippUndef)
	{
		THROW_MTI_RUNTIME_ERROR("Data is not of type Ipp8uArray");
	}

	return _ipp8uArray;
}

Ipp16uArray IpaStreamDataItem::getIpp16uArray() const
{
	// Check for item type
	if (getArrayType() == ippUndef)
	{
		THROW_MTI_RUNTIME_ERROR("Data is not of type Ipp16uArray");
	}

	return _ipp16uArray;
}

Ipp32fArray IpaStreamDataItem::getIpp32fArray() const
{
	// Check for item type
	if (getArrayType() == ippUndef)
	{
		THROW_MTI_RUNTIME_ERROR("Data is not of type Ipp32fArray");
	}

	return _ipp32fArray;
}

Ipp32sArray IpaStreamDataItem::getIpp32sArray() const
{
	// Check for item type
	if (getArrayType() == ippUndef)
	{
		THROW_MTI_RUNTIME_ERROR("Data is not of type Ipp32sArray");
	}

	return _ipp32sArray;
}