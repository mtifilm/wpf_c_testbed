#pragma once
#ifndef ARRAY2DH
#define ARRAY2DH

template <typename T>
class Array2D
{
public:

	Array2D() = default;

	Array2D(int rows, int columns) :
		_rows(rows),
		_columns(columns)
	{
		_data.resize(rows * columns);
	}

	Array2D(const IppiSize &size) : Array2D(size.height, size.width)
	{
		//_data.resize(rows * columns);
	}

	Array2D(const Array2D<T> &&a) :
		_rows(a._rows),
		_columns(a._columns)
	{
		_data = a._data;
	}

	Array2D(const Array2D<T> &a) : _rows(a._rows), _columns(a._columns)
	{
		_data = a._data;
	}

	~Array2D()
	{
	}

	[[nodiscard]] int rows() const
	{
		return _rows;
	}

	[[nodiscard]] int columns() const
	{
		return _columns;
	}

	[[nodiscard]] int sub2ind(int row, int column) const
	{
		return row * _columns + column;
	}

	[[nodiscard]] int sub2ind(const MtiPoint &p) const
	{
		return p.y * _columns + p.x;
	}

	[[nodiscard]] MtiPoint ind2sub(const int index) const
	{
		return { int(index % _columns), int(index / _columns) };
	}

	T const &operator[](const int index) const
	{
		return _data[index];
	}

	T &operator[](const int index)
	{
		return _data[index];
	}

	T &operator[](const MtiPoint &p)
	{
		return _data[sub2ind(p)];
	}

	T &operator()(int row, int column)
	{
		return _data[sub2ind(row, column)];
	}

	// I don't think we need = 
	Array2D<T> &operator=(const Array2D<T> &rhs)
	{
		_rows = rhs._rows;
		_columns = rhs._columns;
		_data = rhs._data;

		return *this;
	}

	Array2D<T> &operator=(Array2D<T> &&rhs) noexcept
	{
		_rows = rhs._rows;
		_columns = rhs._columns;
		_data = rhs._data;

		return *this;
	}

	void set(const T &value)
	{
		for (auto &d : _data)
		{
			d = value;
		}
	}

	[[nodiscard]] int size() const
	{
		return _rows * _columns;
	}

	typename std::vector<T>::iterator begin()
	{
		return _data.begin();
	}

	typename std::vector<T>::iterator end()
	{
		return _data.end();
	}

	[[nodiscard]] typename std::vector<T>::const_iterator begin() const
	{
		return _data.begin();
	}

	[[nodiscard]] typename std::vector<T>::const_iterator end() const
	{
		return _data.end();
	}

	void resize(const MtiSize &size)
	{
		_rows = size.height;
		_columns = size.width;
		_data.resize(_rows * _columns);
	}

	T * data()
	{
		return _data.data();
	}

	[[nodiscard]] std::vector<T> toVector() const
	{
		return _data;
	}

	std::string toString()
	{
		std::ostringstream os;
		for (auto r = 0; r < _rows; r++)
		{
			os << r << ": ";
			for (auto c = 0; c < _columns; c++)
			{
				os << _data[sub2ind(r, c)] << "\t";
			}

			os << std::endl;
		}

		return os.str();
	}

private:
	int _rows = 0;
	int _columns = 0;
	vector<T> _data;
};

#endif // ARRAY2DH

