#pragma once
#include "IppHeaders.h"
#include <map>
#include "SimpleImageFrame.h"

static inline std::mutex g_i_mutex;

// This is a singleton class that does the I/O to DPX files
// There are two fundamental streams
// The input stream from clips
// An output stream from processed files
// There is a third convenience stream that returns output if processes, otherwise input
// This stream was a design mistake and needs to go at a higher level
// TODO: remove it
class CacheFileReader
{
public:
	// Read from cache or file
	SimpleImageFrame getInputFrame(const std::string &fileKey);
	SimpleImageFrame getInputFrame(const std::string& fileKey, const MtiRect& roi);

	// Return empty arrays if not null
	SimpleImageFrame getOutputFrame(const std::string &fileKey);
	SimpleImageFrame getOutputFrame(const std::string& fileKey, const MtiRect& roi);

	// Return the display stream
	SimpleImageFrame getDisplayFrame(const std::string &fileKey);

	// These are just helper functions
	// We should eliminate them or refactor to two CacheReaders
	Ipp16uArray getInputImage(const std::string& fileKey);
	Ipp16uArray getInputAlpha(const string& fileKey);
	Ipp16uArray getInputImage(const std::string& fileKey, const MtiRect& roi);

	Ipp16uArray getDisplayImage(const std::string &fileKey);
	Ipp16uArray getDisplayImage(const std::string& fileKey, const MtiRect& roi);

	Ipp16uArray getOutputImage(const string& fileKey);
	Ipp16uArray getOutputAlpha(const string& fileKey);

	void setOutputFile(const string& fileKey, const Ipp16uArray& outputArray);

	int initCache();
	int clearCache();
	void setPreviewMode(bool previewMode);
	void ClearOutputCache();

	static CacheFileReader *getInstance()
	{
		if (!_instance)
		{
			// To make thread safe 
			const std::lock_guard<std::mutex> lock(g_i_mutex);

			// check again as multiple threads 
			// can reach above step 
			if (_instance == nullptr)
			{
				_instance = new CacheFileReader();
			}
		}
		
		return _instance;
	}

	~CacheFileReader()
	{
		_instance = nullptr;
	}
	
private:
	bool _previewMode;
	std::unordered_map<std::string, SimpleImageFrame> _inputCache;
	std::unordered_map<std::string, SimpleImageFrame> _outputCache;
	inline static CacheFileReader *_instance = nullptr;

	CacheFileReader() = default;
};

