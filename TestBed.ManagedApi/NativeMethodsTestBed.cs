﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using MtiSimple.Core;
using WpfCTestBed.Model;

namespace TestBed.ManagedApi
{
    public static class NativeMethodsTestBed
    {
        const string TestBedInterfaceDll = "TestBedInterface.DLL";

        [DllImport(TestBedInterfaceDll)]
        public static extern int InitCache();

        [DllImport(TestBedInterfaceDll)]
        public static extern void CloseCache();

        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetFileInfo(string fileName, ref SimpleImageDescriptor simpleImageDescriptor);

        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetFileInfoAndData(string fileName, ref SimpleImageDescriptor simpleImageDescriptor, ref IntPtr dataIntPtr);

        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        private static extern int InitToolProcessingNative([In] string fileName, ref MtiRectInt roi, [In] int frames);

        public static int InitializeToolProcessing([In] string fileName, MtiRoi<int> roi, [In] int frames)
        {
            var rect = new MtiRectInt()
                { X = roi.X, Y = roi.Y, Height = roi.Height, Width = roi.Width };

            return InitToolProcessingNative(fileName, ref rect, frames);
        }

        // Preprocessing section ##############################################################
        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        private static extern int InitPreprocessingNative([In] int pass, [In] string[] fileName, int count, ref MtiRectInt rect, [In] int frames);

        public static int InitPreprocessing(int pass, string[] fileNames, MtiRoi<int> processRoi, [In] int frames)
        {
            var rect = new MtiRectInt()
                { X = processRoi.X, Y = processRoi.Y, Height = processRoi.Height, Width = processRoi.Width };

            return InitPreprocessingNative(pass, fileNames, fileNames.Length, ref rect, frames);
        }

        [DllImport(TestBedInterfaceDll, EntryPoint = "CancelPreprocessingAsyncNative", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CancelPreprocessingAsync();

        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        private static extern int PreprocessFrameNative(int pass, [In] string fileName, ref MtiRectInt rect, int index);  

        public static int PreprocessFrame(int pass, string fileName, MtiRoi<int> processRoi, int index)
        {
            var rect = new MtiRectInt()
                { X = processRoi.X, Y = processRoi.Y, Height = processRoi.Height, Width = processRoi.Width };

            return PreprocessFrameNative(pass, fileName, ref rect, index);
        }

        [DllImport(TestBedInterfaceDll, EntryPoint  = "PreprocessingCompletedNative", CallingConvention = CallingConvention.Cdecl)]
        public static extern int PreprocessingCompleted(int pass);

        // Processing section ##############################################################
        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.StdCall)]
        private static extern int InitProcessingNative([In] string[] fileName, int count, ref MtiRectInt rect, [In] int frames, [In] bool saveFrames, [In] string subfolder);

        public static int InitProcessing(string[] fileNames, MtiRoi<int> processRoi, int frames, bool saveFrames, string subfolder)
        {
            var rect = new MtiRectInt()
                { X = processRoi.X, Y = processRoi.Y, Height = processRoi.Height, Width = processRoi.Width };

            return InitProcessingNative(fileNames, fileNames.Length, ref rect, frames, saveFrames, subfolder);
        }

        [DllImport(TestBedInterfaceDll, EntryPoint = "CancelProcessingAsyncNative", CallingConvention = CallingConvention.Cdecl)]
        public static extern int CancelProcessingAsync();

        [DllImport(TestBedInterfaceDll, CallingConvention = CallingConvention.Cdecl)]
        private static extern int ProcessFrameNative([In] string fileName, ref MtiRectInt rect, int index);

        public static int ProcessFrame(string fileName, MtiRoi<int> processRoi, int index)
        {
            var rect = new MtiRectInt()
                { X = processRoi.X, Y = processRoi.Y, Height = processRoi.Height, Width = processRoi.Width };

            return ProcessFrameNative(fileName, ref rect, index);
        }

        [DllImport(TestBedInterfaceDll, EntryPoint = "ProcessingCompletedNative", CallingConvention = CallingConvention.Cdecl)]
        public static extern int ProcessingCompleted();

        [DllImport(TestBedInterfaceDll, EntryPoint = "SetPreviewModeNative", CallingConvention = CallingConvention.Cdecl)]
        public static extern void SetPreviewMode(bool previewMode);
    }
}
